<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 10/12/15
 * Time: 20:50
 */

global $app;

$app->get('/tempat/','getAllTempat');
$app->get('/tempat/:id','getTempat');
$app->get('/tempat/daerah/:id','getTempatByDaerah');
$app->delete('/tempat/:id','deleteTempat');
$app->post('/tempat/','addTempat');
$app->post('/tempat/:id','updateTempat');
$app->post('/tempat/video/:id','updateVideo');

$app->get('/tempat/recomended/:id','getRecomendedTempat');

function getRecomendedTempat($id)
{
    global $app;
    $db = getConnection();
    try{
        $stmt = $db->prepare("SELECT id_daerah, nama_tempat FROM tempat_rekomendasi WHERE id_daerah = ?");
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        Response::out($app, $result, 200);
    }catch (PDOException $e){
        Error::out($app, $e->getMessage(), 500);
    }
}

function updateVideo($id){
    global $app;
    $db = getConnection();

    try{

        $filename = fileUpload();

        $stmt = $db->prepare("UPDATE tempat SET  video = ? WHERE id_tempat = ?");
        $stmt->bindValue(1, $filename);
        $stmt->bindValue(2, $id);
        $stat = $stmt->execute();

        Response::returnResult($app, 'action_update_video',$stat, $id);

    }catch (PDOException $e){
        Error::out($app, $e->getMessage(), 500);
    }catch (Exception $ex){
        Error::out($app, $ex->getMessage() . ' Line : '. $ex->getLine() , 500);
    }
}

function updateTempat($id){
    global $app;
    $db = getConnection();
    $req = $app->request();
    try{
        $stmt = $db->prepare("UPDATE tempat SET  nama_tempat = ?, latitude = ?, longitude = ?, tiket_masuk = ?, fasilitas = ?, biaya_transportasi = ? , keamanan = ?, created_at = ?, id_user = ?, id_daerah = ? WHERE id_tempat = ?");
        $stmt->bindValue(1, $req->post('nama_tempat'));
        $stmt->bindValue(2, $req->post('latitude'));
        $stmt->bindValue(3, $req->post('longitude'));
        $stmt->bindValue(4, $req->post('tiket_masuk'));
        $stmt->bindValue(5, $req->post('fasilitas'));
        $stmt->bindValue(6, $req->post('keamanan'));
        $stmt->bindValue(7, time());
        $stmt->bindValue(8, $req->post('id_user'));
        $stmt->bindValue(9, $req->post('id_daerah'));
        $stmt->bindValue(10, $id);
        $stmt->execute();
        Response::returnResult($app, 'action_update',true, $db->lastInsertId());
    }catch (PDOException $e){
        Error::out($app, $e->getMessage(), 500);
    }catch (Exception $ex){
        Error::out($app, $ex->getMessage() . ' Line : '. $ex->getLine() , 500);
    }
}

function addTempat(){
    global $app;
    $db = getConnection();
    $req = $app->request();

    try{

        $filename = fileUpload();

        $stmt = $db->prepare("INSERT INTO tempat (nama_tempat, latitude, longitude, tiket_masuk, fasilitas, biaya_transportasi , keamanan, video, created_at, id_user, id_daerah) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bindValue(1, $req->post('nama_tempat'));
        $stmt->bindValue(2, $req->post('latitude'));
        $stmt->bindValue(3, $req->post('longitude'));
        $stmt->bindValue(4, $req->post('tiket_masuk'));
        $stmt->bindValue(5, $req->post('fasilitas'));
        $stmt->bindValue(6, $req->post('keamanan'));
        $stmt->bindValue(7, $filename);
        $stmt->bindValue(8, time());
        $stmt->bindValue(9, $req->post('id_user'));
        $stmt->bindValue(10, $req->post('id_daerah'));
        $stmt->execute();
        Response::returnResult($app, 'action_insert',true, $db->lastInsertId());
    }catch (PDOException $e){
        Error::out($app, $e->getMessage(), 500);
    }catch (Exception $ex){
        Error::out($app, $ex->getMessage(), 500);
    }
}

function deleteTempat($id){
    global $app;
    $db = getConnection();
    try{
        $stmt = $db->prepare("DELETE FROM tempat WHERE id_tempat = ?");
        $stmt->bindParam(1, $id);
        $stmt->execute();
        Response::returnResult($app, 'action_delete',true, 0);
    }catch (PDOException $e){
        Error::out($app, $e->getMessage(), 500);
    }
}

function getTempat($id){
    global $app;
    $db = getConnection();
    try{
        $stmt = $db->prepare("SELECT id_tempat, nama_tempat, latitude, longitude, tiket_masuk, fasilitas, biaya_transportasi , keamanan, video, created_at, id_user, id_daerah FROM tempat WHERE id_tempat = ?");
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        Response::out($app, $result, 200);
    }catch (PDOException $e){
        Error::out($app, $e->getMessage(), 500);
    }
}

function getAllTempat(){
    global $app;
    $db = getConnection();
    try{
        $stmt = $db->prepare("SELECT id_tempat, nama_tempat, latitude, longitude, tiket_masuk, fasilitas, biaya_transportasi ,keamanan, video, created_at, id_user, id_daerah FROM tempat ORDER BY id_tempat DESC");
        $stmt->execute();
        $result = $stmt->fetchAll();

        $index = 0;
        $message = array();
        foreach($result as $r){

            $tempat = new Tempat();
            $tempat->id_tempat = (int) $r['id_tempat'];
            $tempat->nama_tempat = $r['nama_tempat'];
            $tempat->latitude = (double) $r['latitude'];
            $tempat->longitude = (double) $r['longitude'];
            $tempat->tiket_masuk = $r['tiket_masuk'];
            $tempat->fasilitas = $r['fasilitas'];
            $tempat->biaya_transportasi = $r['biaya_transportasi'];
            $tempat->keamanan = $r['keamanan'];
            $tempat->video_url = BASE_URL . '/media/'. $r['video'];
            $tempat->created_at = (int) $r['created_at'];

            $stmtx = $db->prepare("select id_user, username from pengguna where id_user = ?");
            $stmtx->bindValue(1, $r['id_user']);
            $stmtx->execute();
            $resx = $stmtx->fetch();
            $tempat->pengguna = $resx;

            $stmtp = $db->prepare("select * from daerah where id_daerah = ?");
            $stmtp->bindValue(1, $r['id_daerah']);
            $stmtp->execute();
            $resp = $stmtp->fetch();
            $tempat->daerah = $resp;
            $message[] = $tempat;
        }
        Response::out($app, $message, 200);
    }catch (PDOException $e){
        Error::out($app, $e->getMessage(), 500);
    }
}

function getTempatByDaerah($id){
     global $app;
    $db = getConnection();
    try{
        $stmt = $db->prepare("SELECT id_tempat, nama_tempat, latitude, longitude, tiket_masuk, fasilitas, biaya_transportasi, keamanan, video, created_at, id_user, id_daerah FROM tempat WHERE id_daerah = ? ORDER BY id_tempat DESC");
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $index = 0;
        $message = array();
        foreach($result as $r){

            $tempat = new Tempat();
            $tempat->id_tempat = (int) $r['id_tempat'];
            $tempat->nama_tempat = $r['nama_tempat'];
            $tempat->latitude = (double) $r['latitude'];
            $tempat->longitude = (double) $r['longitude'];
            $tempat->tiket_masuk = $r['tiket_masuk'];
            $tempat->fasilitas = $r['fasilitas'];
            $tempat->biaya_transportasi = $r['biaya_transportasi'];
            $tempat->keamanan = $r['keamanan'];
            $tempat->video_url = BASE_URL . '/media/'. $r['video'];
            $tempat->created_at = (int) $r['created_at'];

            $stmtx = $db->prepare("select id_user, username from pengguna where id_user = ?");
            $stmtx->bindValue(1, $r['id_user']);
            $stmtx->execute();
            $resx = $stmtx->fetch();
            $tempat->pengguna = $resx;

            $stmtp = $db->prepare("select * from daerah where id_daerah = ?");
            $stmtp->bindValue(1, $r['id_daerah']);
            $stmtp->execute();
            $resp = $stmtp->fetch();
            $tempat->daerah = $resp;
            $message[] = $tempat;
        }
        Response::out($app, $message, 200);
    }catch (PDOException $e){
        Error::out($app, $e->getMessage(), 500);
    }
}

function fileUpload()
{

    if (isset($_FILES['file'])) {
        $errors = array();
        $file_name = $_FILES['file']['name'];
        $file_size = $_FILES['file']['size'];
        $file_tmp = $_FILES['file']['tmp_name'];
        $file_type = $_FILES['file']['type'];
        $file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
        $extensions = array("mp4", "3gp");
        if (in_array($file_ext, $extensions) === false) {
            $errors[] = "image extension not allowed, please choose a JPEG or PNG file.";
        }
        if ($file_size > 2097152) {
            //$errors[]='File size cannot exceed 2 MB';
        }
        if (empty($errors) == true) {
            move_uploaded_file($file_tmp, "media/" . $file_name);
            //echo " uploaded file: " . "media/uploads/" . $file_name;
            return $file_name;
        } else {
            //print_r($errors);
            return null;
        }
    } else {
        $errors = array();
        $errors[] = "No image found";
        //print_r($errors);
        return "-";
    }
}