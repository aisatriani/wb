-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 18, 2016 at 11:14 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.6.23-1+deprecated+dontuse+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wisatabolmong`
--

-- --------------------------------------------------------

--
-- Table structure for table `daerah`
--

CREATE TABLE IF NOT EXISTS `daerah` (
  `id_daerah` int(11) NOT NULL AUTO_INCREMENT,
  `nama_daerah` char(100) NOT NULL,
  PRIMARY KEY (`id_daerah`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `daerah`
--

INSERT INTO `daerah` (`id_daerah`, `nama_daerah`) VALUES
(1, 'kotamobagu'),
(2, 'bolaang mongondow'),
(3, 'bolaang mongondow selatan'),
(4, 'bolaang mongondow timur'),
(5, 'bolaang mongondow utara');

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE IF NOT EXISTS `kecamatan` (
  `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kecamatan` char(100) NOT NULL,
  `id_daerah` int(11) NOT NULL,
  PRIMARY KEY (`id_kecamatan`),
  KEY `id_daerah` (`id_daerah`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id_kecamatan`, `nama_kecamatan`, `id_daerah`) VALUES
(1, 'kotamobagu utara', 1),
(2, 'kotamobagu timur', 1),
(3, 'kotamobagu selatan', 1),
(4, 'kotamobagu barat', 1),
(5, 'Bilalang', 2),
(6, 'Passi Barat', 2),
(7, 'Lolayan', 2),
(8, 'Dumoga Timur', 2),
(9, 'Dumoga Barat', 2),
(10, 'Dumoga Utara', 2),
(11, 'Bolaang', 2),
(12, 'Bolaang Timur', 2),
(13, 'Poigar', 2),
(14, 'Lolak', 2),
(15, 'Sang Tombolang', 2),
(16, 'Bolaang Uki', 3),
(17, 'Posigadan', 3),
(18, 'Pinolosian', 3),
(19, 'Pinolosian Tengah', 3),
(20, 'Pinolosian Timur', 3),
(21, 'Tutuyan', 4),
(22, 'Kotabunan', 4),
(23, 'Nuangan', 4),
(24, 'Modayag', 4),
(25, 'Modayag Barat', 4),
(26, 'Motongkad', 4),
(27, 'Buyat', 4),
(28, 'Mooat', 4),
(29, 'Bintauna', 5),
(30, 'Bolangitang Barat', 5),
(31, 'Bolangitang Timur', 5),
(32, 'Kaidipang', 5),
(33, 'Pinogaluman', 5),
(34, 'Sangkub', 5);

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE IF NOT EXISTS `pengguna` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(100) NOT NULL,
  `password` char(100) NOT NULL,
  `email` char(100) NOT NULL,
  `nama_lengkap` char(150) NOT NULL,
  `id_daerah` int(11) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_daerah` (`id_daerah`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_user`, `username`, `password`, `email`, `nama_lengkap`, `id_daerah`) VALUES
(1, 'admin', '$2y$10$izkBpHSsnF36L3OSQ7vH.evzeE6YZb.EKKDZiP8U2EHTyNWPtcVmO', 'wisata.bolmong@gmail.com', 'administrator', 1),
(2, 'ais', '$2y$10$/AW9Ha9Nz8c.oUjjK0HSFuzUB//CERU0ATUVLYjiQvno9KiP9xTLu', 'aisatriani@plasa.com', 'aisatriani', 1),
(3, 'uyha', '$2y$10$71f4/ZQW9zb1khbwJsOHXO2QDWzijLHNkUOz3tvOp0aaYgfYBQ0hK', 'as@Ã sa.com', 'uyha ziza', 4),
(4, 'test', '$2y$10$RssOljsUFTSxp7gXktA69eHmNAaZ0/mUP/P6b.NXsJEYaGBoLVOzO', 'test@mail.com', 'test users', 4);

-- --------------------------------------------------------

--
-- Table structure for table `tempat`
--

CREATE TABLE IF NOT EXISTS `tempat` (
  `id_tempat` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tempat` char(200) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `tiket_masuk` varchar(500) NOT NULL,
  `fasilitas` varchar(500) NOT NULL,
  `biaya_transportasi` varchar(500) NOT NULL,
  `keamanan` char(200) NOT NULL,
  `video` char(200) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_daerah` int(11) NOT NULL,
  PRIMARY KEY (`id_tempat`),
  KEY `id_daerah` (`id_daerah`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tempat`
--

INSERT INTO `tempat` (`id_tempat`, `nama_tempat`, `latitude`, `longitude`, `tiket_masuk`, `fasilitas`, `biaya_transportasi`, `keamanan`, `video`, `created_at`, `id_user`, `id_daerah`) VALUES
(1, 'Objek wisata pantai babo', 0.8383684523493693, 123.86975803915838, 'Bus/truk :30rb, Mobil: 15rb, Bentor: 7rb, Motor: 5rb', 'Tempat Ibadah, Rumah makan, Pondok untuk wisatawan bersantai, Tempat bilas/Toilet', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Dipantai tersebut keamanan sudah terjamin aman', 'babo.mp4', 1449755077, 1, 2),
(2, 'Objek Wisata Pantai Bungin', 0.8818711723028315, 123.9909911, 'Bus/truk: 25rb, Mobil: 20rb, Motor: 15rb, Sewa Perahu: 15rb', 'Tempat Ibadah, Rumah makan, Pondok untuk wisatawan bersantai,Bisa menyewa perahu nelayan yang ada dipantai tersebut untuk pergi ke pulau molosing, Tempat bilas/Toilet', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Dipantai tersebut sudah terjamin aman', 'bungin.mp4', 1449755106, 1, 2),
(3, 'Objek Wisata Air Panas Bakan', 0.5982705479449921, 124.29321403358223, 'Dewasa: 20rb, Remaja: 15rb, Anak-anak: 10rb', 'Tempat Ibadah, Rumah makan, Kolam renang,Tempat Sauna (mandi uap), Tempat bilas/Toilet', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Ditempat wisata air panas tersebut sudah terjamin aman', 'bakan.mp4', 0, 1, 2),
(4, 'Objek Wisata Pantai Bonur', 0.9108580381897174, 123.28976214209598, 'Bus/truk: 25rb, Mobil: 20rb, Motor: 15rb', 'Rumah makan, Pondok untuk wisatawan bersantai, Tempat bilas/Toilet', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Dipantai tersebut sudah terjamin aman', 'bonur.mp4', 0, 1, 5),
(5, 'Objek Wisata Pantai Pasir Putih', 0.8406102647537081, 123.82239774178626, 'Bus: 25rb, Mobil: 20rb, Motor: 15rb, Sewa perahu: 15rb', 'Ada sebuah gedung terbuka untuk peristirahatan para wisatawan, dan perahu nelayan yang bisa disewa untuk pergi ke pulau tiga.', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Dipantai tersebut sudah terjamin aman.', 'pasirputih.mp4', 0, 1, 2),
(6, 'Objek Wisata Pantai Modisi', 0.38439856588933624, 124.07545196217359, 'Dewasa: 25rb, Remaja: 20rb, Anak-anak:15rb, Bus/truk: 10rb, Mobil: 5rb, Motor: 3rb, Sewa Banana Boat: Dewasa 15 Remaja: 10rb Anak-anak: 5rb /15 menit', 'Tempat Ibadah, Rumah makan, Banana Boat, Pondok untuk wisatawan bersantai, Tempat bilas/Toilet', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Dipantai tersebut sudah terjamin aman', 'modisi.mp4', 0, 1, 3),
(9, 'Objek Wisata Bagasraya', 0.7074167084696497, 124.27227874107955, 'Dewasa: 25rb, Anak-anak: 20rb', 'Kolam renang, Rumah makan, Sport Center, Gedung Serba Guna, dan Tempat bilas/Toilet', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Ditempat tersebut sudah terjamin aman', 'bagasraya.mp4', 0, 1, 1),
(10, 'Objek Wisata Pantai Ompu', 0.9035633333333333, 124.04107, 'Bus/truk: 25rb, Mobil: 20rb, Motor: 15rb', 'Rumah makan, Dermaga tradisional yang terbuat dari kayu dan tempat ini juga memilki banyak spot-spot yang bagus untuk berfoto', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Dipantai tersebut sudah terjamin aman', 'ompu.mp4', 0, 1, 2),
(11, 'Objek Wisata Pantai Pinagut', 0.9201966666666667, 123.26951666666668, 'Bus/truk: 25rb, Mobil: 20rb, Motor:10rb', 'Rumah Makan, Pondok untuk wisatawan bersantai, dan Tempat bilas/Toilet', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Dipantai ini sudah terjamin aman', 'pinagut.mp4', 0, 1, 5),
(13, 'Objek Wisata Gunung Ambang', 0.7685669223057804, 124.44009419477345, 'Belum ada', 'Belum ada', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Keamanan dari para pendaki terjamin', 'ambang.mp4', 0, 1, 4),
(14, 'Objek Wisata Tanjung Woka', 0.9481652161713994, 124.11444313824177, 'Bus/Truk: 30rb, Mobil: 25rb, Motor:20rb', 'Rumah Makan, Pondok untuk wisatawan bersantai, Tempat bilas/Toilet', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Dipantai ini sudah terjamin aman', 'tanjungwoka.mp4', 1456140018, 1, 4),
(15, 'Objek Wisata Tobongon Adventure', 0.8425363100704321, 124.13900915533304, 'Orang:Dewasa: 30rb. Anak-anak: 20rb, Kendaraan: Mobil: 10rb Motor: 5rb, bermain Point Ball /Team: 250rb, Flyin Fox: Dewasa: 50rb Anak-anak: 25rb, Villa /Malam: 300rb', 'Villa, Flyin FOX, Motor ATV, Point Ball, Kolam Renang, Rumah Makan, Tempat bilas/Toilet', 'Sewa mobil 300/hari, Sewa Mobil + Supir 400/hari, Angkutan umum tarif 35000/orang, Sewa bis 1.200.000/hari', 'Ditempat ini sudah terjamin aman', 'tobongon.mp4', 1456140055, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tempat_rekomendasi`
--

CREATE TABLE IF NOT EXISTS `tempat_rekomendasi` (
  `id_tr` int(11) NOT NULL AUTO_INCREMENT,
  `id_daerah` int(11) NOT NULL,
  `nama_tempat` char(255) NOT NULL,
  `keterangan` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_tr`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tempat_rekomendasi`
--

INSERT INTO `tempat_rekomendasi` (`id_tr`, `id_daerah`, `nama_tempat`, `keterangan`) VALUES
(1, 1, 'Kolam Renang Artabandu', '-'),
(2, 1, 'Rumah Adat Bobahidan', '-'),
(3, 1, 'Air Terjun Molibungan', '-'),
(4, 2, 'Pantai Lolan', '-'),
(5, 2, 'Pulau Tiga', '-'),
(6, 2, 'Pulau Molosing', '-'),
(7, 2, 'Air Terjun Mengkang', '-'),
(8, 3, 'Pantai Binika Timur', '-'),
(9, 3, 'Air Terjun Botuliudo', '-'),
(10, 3, 'Pantai Dami', '-'),
(11, 4, 'Pulau Nenas', '-'),
(12, 4, 'Air Terjun Purworejo', '-'),
(13, 4, 'Danau Togid', '-'),
(14, 4, 'Danau Moad', '-'),
(15, 4, 'Danau Motongkad', '-'),
(16, 5, 'Pantai Tanjung Dulang', '-'),
(17, 5, 'Pulai Bongkil', '-'),
(18, 5, 'Air Terjun Pontak', '-'),
(19, 5, 'Air Belanda', '-'),
(20, 5, 'Situs Komalig (Istana Kerajaan Kaidipang Besar)', '-');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_kecamatan`
--
CREATE TABLE IF NOT EXISTS `v_kecamatan` (
`id_kecamatan` int(11)
,`nama_kecamatan` char(100)
,`id_daerah` int(11)
,`nama_daerah` char(100)
);
-- --------------------------------------------------------

--
-- Structure for view `v_kecamatan`
--
DROP TABLE IF EXISTS `v_kecamatan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_kecamatan` AS select `kecamatan`.`id_kecamatan` AS `id_kecamatan`,`kecamatan`.`nama_kecamatan` AS `nama_kecamatan`,`kecamatan`.`id_daerah` AS `id_daerah`,`daerah`.`nama_daerah` AS `nama_daerah` from (`daerah` join `kecamatan` on((`daerah`.`id_daerah` = `kecamatan`.`id_daerah`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
