package app.wisata.bolmong;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import app.wisata.bolmong.model.RekomendasiTempat;

public class DetailRekomendasiActivity extends AppCompatActivity {

    private TextView title;
    private TextView keterangan;
    private RekomendasiTempat rt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_rekomendasi);

        title = (TextView) findViewById(R.id.titleRekomendasi);
        keterangan = (TextView) findViewById(R.id.keterangan);

        handleIntent(getIntent());

    }

    private void handleIntent(Intent intent) {
        rt = (RekomendasiTempat) intent.getSerializableExtra("rt");
        title.setText(rt.getNama_tempat());
        keterangan.setText(rt.getKeterangan());
    }
}
