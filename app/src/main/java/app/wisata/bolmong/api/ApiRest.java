package app.wisata.bolmong.api;

import app.wisata.bolmong.Config;
import retrofit.RestAdapter;

/**
 * Created by aisatriani on 10/12/15.
 */
public class ApiRest {

    public static RestAdapter adapter(){
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Config.API_URL)
                .build();
        return adapter;
    }
}
