package app.wisata.bolmong.api;

import java.util.List;

/**
 * Created by aisatriani on 16/11/15.
 */
public class ApiResponse<T> {
    private int code;
    private boolean status;
    private List<T> items;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public class Result{
        public String action;
        public boolean success;
        public int id;
    }
}
