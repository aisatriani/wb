package app.wisata.bolmong.api;

import app.wisata.bolmong.model.RekomendasiTempat;
import app.wisata.bolmong.model.Tempat;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by aisatriani on 10/12/15.
 */
public interface ApiMethod {

    @GET("/tempat")
    void getTempat(Callback<ApiResponse<Tempat>> callback);

    @GET("/tempat/recomended/{id}")
    void getTempatRecomended(@Path("id") int id_daerah, Callback<ApiResponse<RekomendasiTempat>> callback);

    @GET("/tempat")
    ApiResponse<Tempat> getTempat();


}
