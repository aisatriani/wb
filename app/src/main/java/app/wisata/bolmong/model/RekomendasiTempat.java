package app.wisata.bolmong.model;

import java.io.Serializable;

/**
 * Created by aisatriani on 16/07/16.
 */
public class RekomendasiTempat implements Serializable {

    private int id_tr;
    private int id_daerah;
    private String nama_tempat;
    private String keterangan;


    public int getId_tr() {
        return id_tr;
    }

    public void setId_tr(int id_tr) {
        this.id_tr = id_tr;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public int getId_daerah() {
        return id_daerah;
    }

    public void setId_daerah(int id_daerah) {
        this.id_daerah = id_daerah;
    }

    public String getNama_tempat() {
        return nama_tempat;
    }

    public void setNama_tempat(String nama_tempat) {
        this.nama_tempat = nama_tempat;
    }
}
