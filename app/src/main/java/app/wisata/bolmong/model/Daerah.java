package app.wisata.bolmong.model;

import java.io.Serializable;

/**
 * Created by aisatriani on 11/12/15.
 */
public class Daerah implements Serializable {
    private int id_daerah;
    private String nama_daerah;

    public Daerah(){

    }

    public Daerah(int id_daerah, String nama_daerah){
        this.id_daerah = id_daerah;
        this.nama_daerah = nama_daerah;
    }

    public int getId_daerah() {
        return id_daerah;
    }

    public void setId_daerah(int id_daerah) {
        this.id_daerah = id_daerah;
    }

    public String getNama_daerah() {
        return nama_daerah;
    }

    public void setNama_daerah(String nama_daerah) {
        this.nama_daerah = nama_daerah;
    }
}
