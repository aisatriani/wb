package app.wisata.bolmong;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.*;

import app.wisata.bolmong.api.ApiMethod;
import app.wisata.bolmong.api.ApiResponse;
import app.wisata.bolmong.api.ApiRest;
import app.wisata.bolmong.model.Tempat;
import app.wisata.bolmong.util.DialogUtils;

import com.gc.materialdesign.views.ButtonFloat;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements GoogleMap.InfoWindowAdapter, GoogleMap.OnMapClickListener, View.OnClickListener {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Map<Marker, Tempat> markerTempatMap = new HashMap<>();
    private LayoutInflater inflater;
    private LocationManager locationManager;
    private View infoWindowLayout;
    private PopupWindow popupWindow;
    private Button btn_pick;
    private TextView tv_alamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        infoWindowLayout = inflater.inflate(R.layout.layout_infowindow, null);
        popupWindow = new PopupWindow(infoWindowLayout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(infoWindowLayout);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        tv_alamat = (TextView)findViewById(R.id.tv_alamat);

        btn_pick = (Button) findViewById(R.id.btn_pick);
        btn_pick.setOnClickListener(this);

        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            buildAlertMessageNoGps();
            return;
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLng(Config.DEFAULT_LATLNG));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(final Location location) {
                Preference.saveCurrentLocation(MapsActivity.this, location.getLatitude(), location.getLongitude());
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(location.getLatitude(), location.getLongitude()))
                        .title("current location"));
                if (ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mMap.setMyLocationEnabled(false);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            tv_alamat.setText(getAddress(location.getLatitude(), location.getLongitude()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        loadAllMarker();
        //mMap.setInfoWindowAdapter(this);
        mMap.setOnMapClickListener(this);
    }

    private void loadAllMarker() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu");
        pd.setCanceledOnTouchOutside(false);
        pd.show();
        ApiMethod method = ApiRest.adapter().create(ApiMethod.class);
        method.getTempat(new Callback<ApiResponse<Tempat>>() {
            @Override
            public void success(ApiResponse<Tempat> tempatApiResponse, Response response) {
                pd.dismiss();
                List<Tempat> tempatList = tempatApiResponse.getItems();
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for(Tempat t : tempatList){
                    Marker marker = null;
                    if(t.getDaerah().getId_daerah() == 1){
                        marker = mMap.addMarker(new MarkerOptions()
                                .title(t.getNama_tempat())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marina_green))
                                .snippet(t.getDaerah().getNama_daerah())
                                .position(new LatLng(t.getLatitude(), t.getLongitude()))
                        );
                    }else if(t.getDaerah().getId_daerah() == 2){

                        marker = mMap.addMarker(new MarkerOptions()
                                .title(t.getNama_tempat())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marina_yellow))
                                .snippet(t.getDaerah().getNama_daerah())
                                .position(new LatLng(t.getLatitude(), t.getLongitude()))
                        );

                    }else if(t.getDaerah().getId_daerah() == 3){

                        marker = mMap.addMarker(new MarkerOptions()
                                .title(t.getNama_tempat())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marina_violet))
                                .snippet(t.getDaerah().getNama_daerah())
                                .position(new LatLng(t.getLatitude(), t.getLongitude()))
                        );

                    }else if(t.getDaerah().getId_daerah() == 4){

                        marker = mMap.addMarker(new MarkerOptions()
                                .title(t.getNama_tempat())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marina_blue))
                                .snippet(t.getDaerah().getNama_daerah())
                                .position(new LatLng(t.getLatitude(), t.getLongitude()))
                        );

                    }else if(t.getDaerah().getId_daerah() == 5){

                        marker = mMap.addMarker(new MarkerOptions()
                                .title(t.getNama_tempat())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marina_red))
                                .snippet(t.getDaerah().getNama_daerah())
                                .position(new LatLng(t.getLatitude(), t.getLongitude()))
                        );

                    }


                    markerTempatMap.put(marker, t);
                    builder.include(marker.getPosition());
                    //bounceMarker(marker);
                    //marker.showInfoWindow();
                }
                builder.include(Preference.getCurrentLocation(MapsActivity.this));
                LatLngBounds bounds = builder.build();
                int w = getResources().getDisplayMetrics().widthPixels;
                int h = getResources().getDisplayMetrics().heightPixels;
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,w,h,100));
                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
//                        marker.hideInfoWindow();
                        final Tempat eTempat = markerTempatMap.get(marker);
                        ImageView icon = (ImageView)infoWindowLayout.findViewById(R.id.img_infowindow_icon);
                        TextView tv_title = (TextView)infoWindowLayout.findViewById(R.id.tv_infowindow_title);
                        TextView tv_location =(TextView)infoWindowLayout.findViewById(R.id.tv_infowindow_daerah);
                        Button btn_details = (Button)infoWindowLayout.findViewById(R.id.btn_infowindow_details);

                        tv_title.setText(eTempat.getNama_tempat());
                        tv_location.setText(eTempat.getDaerah().getNama_daerah());

                        btn_details.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent intent = new Intent(MapsActivity.this, VideoActivity.class);
                                intent.putExtra("tempat",eTempat);
                                startActivity(intent);
                            }
                        });

                        popupWindow.showAtLocation(findViewById(R.id.map), Gravity.CENTER,10, 10);

                        return true;
                    }
                });

            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                //DialogUtils.showDialogInfo(MapsActivity.this, "Terjadi kesalahan",error.getMessage());
                showDialogError("Terjadi Kesalahan",error.getMessage());
            }
        });
    }

    public void showDialogError(String title, String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setUpMap();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showVideoDialog(Tempat eTempat) {
        View v = inflater.inflate(R.layout.layout_dialog_video, null);
        VideoView videoView = (VideoView)v.findViewById(R.id.videoView);

        String vidAddress = eTempat.getVideo();
        Uri vidUri = Uri.parse(vidAddress);
        videoView.setVideoURI(vidUri);

        MediaController vidControl = new MediaController(this);
        vidControl.setAnchorView(videoView);
        videoView.setMediaController(vidControl);

        videoView.start();

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(v)
                .setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .create();

        dialog.show();

    }

    private void buildAlertMessageNoGps() {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("Fitur GPS dalam keadaan tidak aktiv, Untuk melanjutkan kepengaturan lokasi pilih opsi YA.")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        //isSetGPS = true;
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        //buildAlertMode();
                        //finish();
                    }
                });
        final android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean bounceMarker(final Marker marker){
        final Handler handler = new Handler();

        final long startTime = SystemClock.uptimeMillis();
        final long duration = 2000;

        Projection proj = mMap.getProjection();
        final LatLng markerLatLng = marker.getPosition();
        Point startPoint = proj.toScreenLocation(markerLatLng);
        startPoint.offset(0, -100);
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);

        final Interpolator interpolator = new BounceInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - startTime;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * markerLatLng.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * markerLatLng.latitude + (1 - t) * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });

        //return false; //have not consumed the event
        return true; //have consumed the event

    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        ImageView icon = (ImageView)infoWindowLayout.findViewById(R.id.img_infowindow_icon);
        TextView tv_title = (TextView)infoWindowLayout.findViewById(R.id.tv_infowindow_title);
        TextView tv_location =(TextView)infoWindowLayout.findViewById(R.id.tv_infowindow_daerah);
        Button btn_details = (Button)infoWindowLayout.findViewById(R.id.btn_infowindow_details);

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("image click window");
            }
        });

        final Tempat eTempat = markerTempatMap.get(marker);
        tv_title.setText(eTempat.getNama_tempat());
        tv_location.setText(eTempat.getDaerah().getNama_daerah());

        btn_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MapsActivity.this, VideoActivity.class);
                intent.putExtra("tempat",eTempat);
                startActivity(intent);
            }
        });

        return infoWindowLayout;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if(popupWindow.isShowing()){
            popupWindow.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        if(popupWindow.isShowing()){
            popupWindow.dismiss();
            return;
        }

        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.btn_pick){
            Intent i = new Intent(this, PickLocation.class);
            startActivityForResult(i, 200);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 200 && resultCode == RESULT_OK){
            mMap.clear();
            //mMap.setOnMyLocationChangeListener(null);
            final double lat = data.getDoubleExtra("latitude",0);
            final double lan = data.getDoubleExtra("longitude",0);
            Preference.saveCurrentLocation(this, lat, lan);

            loadAllMarker();
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, lan))

                    .title("current location"));

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        tv_alamat.setText(getAddress(lat, lan));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }

    public String getAddress(double latitude, double longitude) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5


            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            return String.format("%s, %s", address, city);



        //return String.format("%s, %s", "Unknown", "");

    }
}
