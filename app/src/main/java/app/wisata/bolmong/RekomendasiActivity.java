package app.wisata.bolmong;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import app.wisata.bolmong.api.ApiMethod;
import app.wisata.bolmong.api.ApiResponse;
import app.wisata.bolmong.api.ApiRest;
import app.wisata.bolmong.model.Daerah;
import app.wisata.bolmong.model.RekomendasiTempat;
import app.wisata.bolmong.widget.NonScrollListView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RekomendasiActivity extends AppCompatActivity {

    private static final String TAG = RekomendasiActivity.class.getCanonicalName();
    private NonScrollListView list_rekomendasi;
    private Daerah mDaerah;
    private List<String> mData = new ArrayList<>();
    private TextView title_rekomendasi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekomendasi);

        list_rekomendasi = (NonScrollListView) findViewById(R.id.lv_detail_rekomendasi);
        title_rekomendasi = (TextView) findViewById(R.id.title_rekomendasi);

        handleIntent(getIntent());
        loadRekomendasi();
    }

    private void handleIntent(Intent intent) {
        if(intent.getExtras() != null){
            mDaerah = (Daerah)intent.getSerializableExtra("daerah");
            title_rekomendasi.setText("Rekomendasi tempat wisata "+ mDaerah.getNama_daerah());
        }
    }

    private void loadRekomendasi() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("tunggu sebentar");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        ApiMethod api = ApiRest.adapter().create(ApiMethod.class);
        api.getTempatRecomended(mDaerah.getId_daerah(), new Callback<ApiResponse<RekomendasiTempat>>() {
            @Override
            public void success(ApiResponse<RekomendasiTempat> rekomendasiTempatApiResponse, Response response) {
                pd.dismiss();
                List<RekomendasiTempat> items = rekomendasiTempatApiResponse.getItems();
                int x = 0;
                for (RekomendasiTempat rt: items) {
                    mData.add(rt.getNama_tempat());
                    Log.d(TAG, "success: "+ rt.getNama_tempat());

                    x++;
                }

                String[] data = new String[mData.size()];
                data = mData.toArray(data);
                final ArrayAdapter<RekomendasiTempat> adapter = arrayAdapter(android.R.drawable.checkbox_on_background,items);
                list_rekomendasi.setAdapter(adapter);

                list_rekomendasi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        RekomendasiTempat rt = adapter.getItem(position);
                        Intent i = new Intent(RekomendasiActivity.this, DetailRekomendasiActivity.class);
                        i.putExtra("rt", rt);
                        startActivity(i);

                    }
                });

            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
            }
        });
    }



    public ArrayAdapter<RekomendasiTempat> arrayAdapter(final int resIcon, List<RekomendasiTempat> data){

        ArrayAdapter<RekomendasiTempat> arAdapter = new ArrayAdapter<RekomendasiTempat>(this, android.R.layout.activity_list_item, data){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null){
                    convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.activity_list_item, parent, false);
                }
                RekomendasiTempat str = getItem(position);
                TextView textView = (TextView)convertView.findViewById(android.R.id.text1);
                ImageView imageView = (ImageView)convertView.findViewById(android.R.id.icon);
                textView.setText(str.getNama_tempat());
                imageView.setImageResource(resIcon);
                return convertView;
            }
        };

        return arAdapter;

    }
}
