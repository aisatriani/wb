package app.wisata.bolmong;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by aisatriani on 26/09/15.
 */
public class Preference {
    public static final String PREF_NAME = "pref_app";
    public static final String PREF_LATITUDE = "pref_latitude";
    public static final String PREF_LONGITUDE = "pref_longitude";
    public static final String PREF_ISLOGIN = "pref_islogin";



    public static void saveCurrentLocation(Context context, double latitude, double longitude){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(PREF_LATITUDE, Double.doubleToLongBits(latitude));
        editor.putLong(PREF_LONGITUDE, Double.doubleToLongBits(longitude));
        editor.commit();
    }


    public static void saveLoginState(Context context, boolean loginIn){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_ISLOGIN, loginIn);
        editor.commit();
    }

    public static boolean isLoginIn(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        boolean loginIn = sharedPreferences.getBoolean(PREF_ISLOGIN, false);
        return loginIn;
    }

    public static LatLng getCurrentLocation(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        double lat =Double.longBitsToDouble(sharedPreferences.getLong(PREF_LATITUDE, 0));
        double lon =Double.longBitsToDouble(sharedPreferences.getLong(PREF_LONGITUDE,0));
        return new LatLng(lat, lon);
    }

    public static double getCurrentLatitude(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        double lat =Double.longBitsToDouble(sharedPreferences.getLong(PREF_LATITUDE, 0));

        return lat;
    }

    public static double getCurrentLongitude(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        double lon =Double.longBitsToDouble(sharedPreferences.getLong(PREF_LONGITUDE,0));
        return lon;
    }

}

