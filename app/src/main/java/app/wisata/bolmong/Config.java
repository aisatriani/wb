package app.wisata.bolmong;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by aisatriani on 10/12/15.
 */
public class Config {

    //public static final String API_URL = "http://192.168.56.1/wisatabolmong";
    //public static final String API_URL = "http://192.168.2.128/wisatabolmong";
    public static final String API_URL = "http://wb.tenilodev.com";
    public static final LatLng DEFAULT_LATLNG = new LatLng(0.620487, 123.852310);

}
