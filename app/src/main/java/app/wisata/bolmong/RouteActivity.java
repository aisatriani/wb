package app.wisata.bolmong;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import app.wisata.bolmong.model.Tempat;
import app.wisata.bolmong.util.GMapV2Direction;
import app.wisata.bolmong.util.GeoUtils;

import com.gc.materialdesign.views.ButtonFloat;
import com.gc.materialdesign.widgets.ProgressDialog;
import com.gc.materialdesign.widgets.SnackBar;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;
import com.squareup.okhttp.Route;

import org.w3c.dom.Document;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class RouteActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Tempat mTempat;
    private ProgressDialog pd;
    private LinearLayout layout_details;
    private TextView route_distance;
    private TextView route_time;
    private ButtonFloat buttonHome;
    private boolean isDirection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        buttonHome = (ButtonFloat)findViewById(R.id.bfHome);
        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RouteActivity.this, MapsActivity.class));
                finish();
            }
        });

        handleIntent(getIntent());
        setUpMapIfNeeded();

    }

    private void handleIntent(Intent intent) {

        if(intent.getExtras() != null){
            mTempat = (Tempat)intent.getSerializableExtra("tempat");
            layout_details = (LinearLayout) findViewById(R.id.route_details);
            route_distance = (TextView) findViewById(R.id.route_distance);
            route_time = (TextView) findViewById(R.id.route_time);
            layout_details.setVisibility(View.GONE);

            isDirection = intent.getBooleanExtra("direction", false);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        Marker markerTarget = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(mTempat.getLatitude(), mTempat.getLongitude()))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marina))
                .title(mTempat.getNama_tempat()));
        markerTarget.showInfoWindow();

        Marker markerKu = mMap.addMarker(new MarkerOptions()
                .position(Preference.getCurrentLocation(this))
                .title("Lokasi ku"));

        builder.include(markerTarget.getPosition());
        builder.include(markerKu.getPosition());

        LatLngBounds bounds = builder.build();

        int w = getResources().getDisplayMetrics().widthPixels;
        int h = getResources().getDisplayMetrics().heightPixels;
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,w,h,100));

        if(!isDirection) {
            drawLineHaversine();
        }else{

            AsynDirection direction = new AsynDirection(this);
            direction.execute();

            pd = new ProgressDialog(this,"memuat..");
            pd.setCanceledOnTouchOutside(false);
            pd.show();

        }



    }

    private void drawLineHaversine() {
        LatLng start = Preference.getCurrentLocation(this);
        LatLng end = new LatLng(mTempat.getLatitude(),mTempat.getLongitude());

        List<LatLng> latLngList = new ArrayList<>();
        latLngList.add(start);
        latLngList.add(end);

        double jarak = GeoUtils.distance(start.latitude, start.longitude, end.latitude, end.longitude);
        mMap.addPolyline(new PolylineOptions().addAll(latLngList).width(8.0f).color(Color.BLUE));

        jarak = jarak * 0.001;

        DecimalFormat df = new DecimalFormat("#.##");
        String val = df.format(jarak);



        layout_details.setVisibility(View.VISIBLE);
        route_distance.setText(String.format("Jarak Haversine : %s Km", val));
        //route_time.setText(String.format("Waktu tempuh : %s", waktu));

    }

    private class AsynDirection extends AsyncTask<Void,Void,Document>{

        private final Context context;

        public AsynDirection(Context ctx){
            this.context = ctx;
        }

        @Override
        protected Document doInBackground(Void... voids) {
            GMapV2Direction gMapV2Direction = new GMapV2Direction();
            LatLng start = Preference.getCurrentLocation(context);
            LatLng end = new LatLng(mTempat.getLatitude(),mTempat.getLongitude());
            Document document = gMapV2Direction.getDocument(start,end, GMapV2Direction.MODE_DRIVING);
            return document;
            //return gMapV2Direction.getDirection(document);
        }

        @Override
        protected void onPostExecute(Document doc) {
            super.onPostExecute(doc);
            if(pd.isShowing()){
                pd.dismiss();
            }

            try {

                ArrayList<LatLng> latLngs = GMapV2Direction.getDirection(doc);
                mMap.addPolyline(new PolylineOptions().addAll(latLngs).width(8.0f).color(Color.BLUE));
                String jarak = GMapV2Direction.getDistanceText(doc);
                String waktu = GMapV2Direction.getDurationText(doc);

                layout_details.setVisibility(View.VISIBLE);
                route_distance.setText(String.format("Total Jarak : %s",jarak));
                route_time.setText(String.format("Waktu tempuh : %s", waktu));

            }catch (NullPointerException e){

                SnackBar snackBar = new SnackBar(RouteActivity.this, "Gagal memuat data", "retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                snackBar.setIndeterminate(true);
                snackBar.show();

            }



        }
    }
}
