package app.wisata.bolmong;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.*;
import android.widget.*;

import app.wisata.bolmong.model.RekomendasiTempat;
import app.wisata.bolmong.model.Tempat;
import app.wisata.bolmong.video.ResizeSurfaceView;
import app.wisata.bolmong.video.VideoControllerView;
import com.gc.materialdesign.views.*;
import com.gc.materialdesign.views.ScrollView;
import com.getbase.floatingactionbutton.FloatingActionButton;

import org.w3c.dom.Text;

import java.io.IOException;

public class VideoActivity extends AppCompatActivity implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, VideoControllerView.MediaPlayerControlListener, MediaPlayer.OnVideoSizeChangedListener, View.OnClickListener {

    private final static String TAG = "VideoActivity";
    ResizeSurfaceView mVideoSurface;
    MediaPlayer mMediaPlayer;
    VideoControllerView controller;
    private int mVideoWidth;
    private int mVideoHeight;
    private View mContentView;
    private View mLoadingView;
    private Tempat mTempat;
    private TextView tv_title;
    private TextView tv_location;
    private ListView lv_fasilitas;
    private ListView lv_tiket;
    private ListView lv_biaya_transportasi;
    private LayoutInflater inflater;
    private TextView tv_keamanan;
    private ScrollView scroll;
    private FloatingActionButton action_home;
    private FloatingActionButton action_direction;
    private FloatingActionButton action_haversine;
    private FloatingActionButton action_rekomendasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        handleIntent(getIntent());

        action_home = (FloatingActionButton) findViewById(R.id.action_home);
        action_direction = (FloatingActionButton) findViewById(R.id.action_direction);
        action_haversine = (FloatingActionButton) findViewById(R.id.action_haversine);
        action_rekomendasi = (FloatingActionButton) findViewById(R.id.action_recomended);

        action_home.setOnClickListener(this);
        action_direction.setOnClickListener(this);
        action_haversine.setOnClickListener(this);
        action_rekomendasi.setOnClickListener(this);

        initVIewDetail();

        mVideoSurface = (ResizeSurfaceView) findViewById(R.id.videoSurface);
        mContentView = findViewById(R.id.video_container);
        mLoadingView = findViewById(R.id.loading);
        SurfaceHolder videoHolder = mVideoSurface.getHolder();
        videoHolder.addCallback(this);

            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setOnVideoSizeChangedListener(this);
            controller = new VideoControllerView(this);
            mLoadingView.setVisibility(View.VISIBLE);

            try {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.setDataSource(this, Uri.parse(mTempat.getVideo()));
                mMediaPlayer.setOnPreparedListener(this);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        mVideoSurface.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                controller.toggleContollerView();
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnVideoSizeChangedListener(this);
        controller = new VideoControllerView(this);
        mLoadingView.setVisibility(View.VISIBLE);

        try {
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setDataSource(this, Uri.parse(mTempat.getVideo()));
            mMediaPlayer.setOnPreparedListener(this);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleIntent(Intent intent) {
        if(intent.getExtras() != null){
            mTempat = (Tempat)intent.getSerializableExtra("tempat");
            setTitle(mTempat.getNama_tempat());
            Log.d(TAG, "handleIntent: "+ mTempat.getVideo());
        }
    }

    private void initVIewDetail() {
        inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);

        tv_title = (TextView)findViewById(R.id.tv_detail_title);
        tv_location = (TextView)findViewById(R.id.tv_detail_location);
        lv_fasilitas = (ListView)findViewById(R.id.lv_detail_fasilitas);
        lv_tiket = (ListView)findViewById(R.id.lv_detail_tiket);
        tv_keamanan = (TextView)findViewById(R.id.tv_detail_keamanan);
        lv_biaya_transportasi = (ListView)findViewById(R.id.lv_detail_biaya_transportasi);



        scroll = (com.gc.materialdesign.views.ScrollView)findViewById(R.id.scroll);

        tv_title.setText(mTempat.getNama_tempat());
        tv_location.setText(mTempat.getDaerah().getNama_daerah());


        String[] fasilitas = TextUtils.split(mTempat.getFasilitas(), "\\s*,\\s*");
        ArrayAdapter adapterFasilitas = arrayAdapter(android.R.drawable.checkbox_on_background, fasilitas);
        lv_fasilitas.setAdapter(adapterFasilitas);


        String[] tiket = TextUtils.split(mTempat.getTiket_masuk(), "\\s*,\\s*");
        ArrayAdapter adapterTiket = arrayAdapter(android.R.drawable.radiobutton_on_background, tiket);
        lv_tiket.setAdapter(adapterTiket);

        Log.e("biaya_transport", mTempat.getBiaya_transportasi());

        String[] biaya = TextUtils.split(mTempat.getBiaya_transportasi(), "\\s*,\\s*");
        ArrayAdapter adapterBiaya = arrayAdapter(android.R.drawable.radiobutton_on_background, biaya);
        lv_biaya_transportasi.setAdapter(adapterBiaya);


        tv_keamanan.setText(mTempat.getKeamanan());



    }

    public ArrayAdapter<String> arrayAdapter(final int resIcon, String[] data){

        ArrayAdapter<String> arAdapter = new ArrayAdapter<String>(this, android.R.layout.activity_list_item, data){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null){
                    convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.activity_list_item, parent, false);
                }
                String str = getItem(position);
                TextView textView = (TextView)convertView.findViewById(android.R.id.text1);
                ImageView imageView = (ImageView)convertView.findViewById(android.R.id.icon);
                textView.setText(str);
                imageView.setImageResource(resIcon);
                return convertView;
            }
        };

        return arAdapter;

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        controller.show();
        return false;
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        mVideoHeight = mp.getVideoHeight();
        mVideoWidth = mp.getVideoWidth();
        if (mVideoHeight > 0 && mVideoWidth > 0){
            mVideoSurface.adjustSize(mContentView.getWidth(), mContentView.getHeight(), 640, 480);
            //mVideoSurface.adjustSize(mContentView.getWidth(), mContentView.getHeight(), mMediaPlayer.getVideoWidth(), mMediaPlayer.getVideoHeight());
            Log.d(TAG, "lebar video size changed " + mMediaPlayer.getVideoWidth());
            Log.d(TAG, "tunggu video size changed " + mMediaPlayer.getVideoHeight());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mVideoWidth > 0 && mVideoHeight > 0)
            mVideoSurface.adjustSize(getDeviceWidth(this),getDeviceHeight(this),mVideoSurface.getWidth(), mVideoSurface.getHeight());
    }

    private void resetPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    public static int getDeviceWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(mDisplayMetrics);
        return mDisplayMetrics.widthPixels;
    }

    public static int getDeviceHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(mDisplayMetrics);
        return mDisplayMetrics.heightPixels;
    }


    // Implement SurfaceHolder.Callback
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if(mMediaPlayer != null) {
            mMediaPlayer.setDisplay(holder);
            mMediaPlayer.prepareAsync();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
          resetPlayer();
    }
// End SurfaceHolder.Callback


// Implement MediaPlayer.OnPreparedListener
    @Override
    public void onPrepared(MediaPlayer mp) {
        //setup video controller view
        controller.setMediaPlayerControlListener(this);
        mLoadingView.setVisibility(View.GONE);
        mVideoSurface.setVisibility(View.VISIBLE);
        controller.setAnchorView((FrameLayout) findViewById(R.id.videoSurfaceContainer));
        controller.setGestureListener(this);
        mMediaPlayer.start();
    }
// End MediaPlayer.OnPreparedListener

    /**
     * Implement VideoMediaController.MediaPlayerControl
      */
    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekProgress() {
        return true;
    }


    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
         if(null != mMediaPlayer)
           return mMediaPlayer.getCurrentPosition();
        else
           return 0;
    }

    @Override
    public int getDuration() {
        if(null != mMediaPlayer)
            return mMediaPlayer.getDuration();
        else
            return 0;
    }

    @Override
    public boolean isPlaying() {
        if(null != mMediaPlayer)
            return mMediaPlayer.isPlaying();
        else
            return false;
    }

    @Override
    public void pause() {
        if(null != mMediaPlayer) {
            mMediaPlayer.pause();
        }

    }

    @Override
    public void seekTo(int i) {
        if(null != mMediaPlayer) {
            mMediaPlayer.seekTo(i);
        }
    }

    @Override
    public void start() {
        if(null != mMediaPlayer) {
            mMediaPlayer.start();
        }
    }

    @Override
    public boolean isFullScreen() {
        return getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE ? true : false;
    }

    @Override
    public void toggleFullScreen() {
       if(isFullScreen()){
           setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
       }else {
           setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
       }
    }

    @Override
    public void exit() {
        resetPlayer();
        finish();
    }

    @Override
    public String getTopTitle() {
        return "buck bunny".toUpperCase();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.action_haversine){
            Intent intent = new Intent(this, RouteActivity.class);
            intent.putExtra("tempat",mTempat);
            intent.putExtra("direction",false);
            startActivity(intent);
        }
        if(id == R.id.action_direction){
            Intent intent = new Intent(this, RouteActivity.class);
            intent.putExtra("tempat",mTempat);
            intent.putExtra("direction",true);
            startActivity(intent);
        }
        if(id == R.id.action_recomended){
            Intent intent = new Intent(this, RekomendasiActivity.class);
            intent.putExtra("daerah",mTempat.getDaerah());
            startActivity(intent);
        }
        if(id == R.id.action_home){
            startActivity(new Intent(this, MapsActivity.class));
            finish();
        }

    }
    // End VideoMediaController.MediaPlayerControl

}
