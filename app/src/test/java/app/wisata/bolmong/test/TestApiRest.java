package app.wisata.bolmong.test;

import app.wisata.bolmong.api.ApiMethod;
import app.wisata.bolmong.api.ApiResponse;
import app.wisata.bolmong.api.ApiRest;
import app.wisata.bolmong.model.Tempat;
import org.junit.Assert;
import org.junit.Test;
import retrofit.client.OkClient;
import retrofit.client.Request;

import java.io.IOException;

/**
 * Created by aisatriani on 10/12/15.
 */
public class TestApiRest {

    @Test
    public void testGetTempat(){
        OkClient client = new OkClient();
        Request request = new Request("get","http://google.com",null,null);
        try {
            client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
