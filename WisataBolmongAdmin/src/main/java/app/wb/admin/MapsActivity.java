package app.wb.admin;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import app.wb.admin.fragment.MainFragment;
import app.wb.admin.fragment.MapFragment;
import app.wb.admin.model.Users;
import app.wb.admin.utils.DialogUtils;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends BaseActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Users userdata;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_main_admin;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if(!Preference.isLogin(this)){
            startActivity(new Intent(this, IntroActivity.class));
            finish();
            return;
        }

        userdata = Preference.getUserData(this);
        if (BuildConfig.DEBUG) Log.d("MapsActivity", "userdata login "+userdata.getNama_lengkap());

        TextView fitur_list = (TextView)findViewById(R.id.welcome_fitur_text);

        String html = "" +
                "Menampilkan Wisata Dalam Bentuk pemetaan digital\n" +
                "Fitur jarak dan waktu tempuh dari tujuan asal ke obyek wisata dengan otomatis\n" +
                "";
        Spanned htmlList = Html.fromHtml(html);
        fitur_list.setText(html);

        //setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(AppConfig.LATLNG_DEFAULT,9));
        mMap.addMarker(new MarkerOptions().position(AppConfig.LATLNG_DEFAULT).title("Marker"));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLngx) {

                final LatLng latLng = latLngx;

                MapsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        double latitude = latLng.latitude;
                        double longitude = latLng.longitude;

                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(MapsActivity.this, Locale.getDefault());

                        try {
                            // Only if available else return NULL
                            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();
                            String knownName = addresses.get(0).getFeatureName();

                            if (BuildConfig.DEBUG) Log.d("MapsActivity", address);

                            DialogUtils.showInfoDialog(MapsActivity.this, postalCode);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }
                });



            }
        });
    }

    @Override
    public boolean onItemClick(View view, int i, IDrawerItem iDrawerItem) {

        Log.i("drawer item click", String.valueOf(i));
        Log.i("drawer identifier click", String.valueOf(iDrawerItem.getIdentifier()));

        if(i != getSelected_position()){
            if(iDrawerItem.getIdentifier() == IDENT_HOME){
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new MainFragment())
                        .commit();
            }
            if(iDrawerItem.getIdentifier() == IDENT_USERS){
                startActivity(new Intent(this, UserActivity.class));
            }
            if(iDrawerItem.getIdentifier() == IDENT_DAERAH){
                startActivity(new Intent(this, DaerahActivity.class));
            }
            if(iDrawerItem.getIdentifier() == IDENT_TEMPAT){
                startActivity(new Intent(this, TempatWisataActivity.class));
            }
            if(iDrawerItem.getIdentifier() == IDENT_LOGOUT){
                Preference.setLogin(MapsActivity.this, false);
                startActivity(new Intent(this, IntroActivity.class));
                this.finish();
            }

        }

        return super.onItemClick(view, i, iDrawerItem);
    }
}
