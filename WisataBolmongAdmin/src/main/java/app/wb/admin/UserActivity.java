package app.wb.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.melnykov.fab.FloatingActionButton;

import app.wb.admin.adapter.UserAdapter;
import app.wb.admin.api.MethodApi;
import app.wb.admin.api.RestApi;
import app.wb.admin.api.ResultApi;
import app.wb.admin.model.Users;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class UserActivity extends BaseActivity {

    private static final int REQ_CODE = 200;
    private ListView listUsers;
    private FloatingActionButton fab;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_user;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        listUsers = (ListView) findViewById(R.id.listViewUsers);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.attachToListView(listUsers);
        fab.show();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(UserActivity.this, FormUsersActivity.class), REQ_CODE);
            }
        });

        loadListFromServer();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQ_CODE && resultCode == RESULT_OK){
            loadListFromServer();
        }
    }

    private void loadListFromServer() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.show();

        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        method.getUsers(new Callback<ResultApi<Users>>() {
            @Override
            public void success(ResultApi<Users> usersResultApi, Response response) {

                UserAdapter adapter = new UserAdapter(UserActivity.this, usersResultApi.getItems());
                listUsers.setAdapter(adapter);
                pd.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                RestApi.handleError(UserActivity.this, error);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
