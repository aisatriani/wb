package app.wb.admin.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import app.wb.admin.R;
import app.wb.admin.model.Users;

public class UserAdapter extends BaseAdapter {

    private List<Users> objects;

    private Context context;
    private LayoutInflater layoutInflater;

    public UserAdapter(Context context, List<Users> usersList) {
        this.context = context;
        this.objects = usersList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Users getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_listview_2r, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews((Users)getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(Users users, ViewHolder holder) {
        holder.textViewTitle.setText(users.getNama_lengkap());
        holder.textViewDesc.setText(users.getEmail());
        holder.imageViewIcon.setImageResource(R.drawable.ic_icon_user);
    }

    protected class ViewHolder {
        private ImageView imageViewIcon;
    private TextView textViewTitle;
    private TextView textViewDesc;

        public ViewHolder(View view) {
            imageViewIcon = (ImageView) view.findViewById(R.id.imageViewIcon);
            textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
            textViewDesc = (TextView) view.findViewById(R.id.textViewDesc);
        }
    }
}
