package app.wb.admin.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.wb.admin.R;
import app.wb.admin.model.Daerah;
import app.wb.admin.model.Tempat;

public class TempatAdapter extends BaseAdapter {

    private List<Tempat> objects;

    private Context context;
    private LayoutInflater layoutInflater;

    public TempatAdapter(Context context, List<Tempat> usersList) {
        this.context = context;
        this.objects = usersList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Tempat getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_listview_2r, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews((Tempat)getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(Tempat tempat, ViewHolder holder) {
        holder.textViewTitle.setText(tempat.getNama_tempat());
        holder.textViewDesc.setText(tempat.getDaerah().getNama_daerah());
        holder.imageViewIcon.setImageResource(R.drawable.ic_action_glasses);
    }


    protected class ViewHolder {
        private ImageView imageViewIcon;


    private TextView textViewTitle;
    private TextView textViewDesc;

        public ViewHolder(View view) {
            imageViewIcon = (ImageView) view.findViewById(R.id.imageViewIcon);
            textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
            textViewDesc = (TextView) view.findViewById(R.id.textViewDesc);
        }
    }
}
