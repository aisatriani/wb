package app.wb.admin;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import app.wb.admin.api.MethodApi;
import app.wb.admin.api.ResponseApi;
import app.wb.admin.api.RestApi;
import app.wb.admin.model.Daerah;
import app.wb.admin.model.Tempat;
import app.wb.admin.model.Users;
import app.wb.admin.utils.DialogUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public class FormTempatActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener {

    private static final int REQ_CODE_VIDEO = 101;
    private GoogleMap mMap;
    private EditText etNamaTempat;
    private EditText etlatitude;
    private EditText etLongitude;
    private EditText etTiket;
    private EditText etFasilitas;
    private EditText etKeamanan;
    private EditText etVideo;
    private Button buttonSubmit;
    private ScrollView mainScrollView;
    private ImageView transparentImageView;
    private Uri outputFileUri;
    private String videoPath;
    private Users user;
    private boolean isEditMode;
    private Tempat tempat;
    private CardView cvVideo;
    private Daerah daerah;
    private EditText etBiayaTrans;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_form_tempat;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        user = Preference.getUserData(this);

        etNamaTempat = (EditText) findViewById(R.id.etNamaTempat);
        etlatitude = (EditText) findViewById(R.id.etLatitude);
        etLongitude = (EditText) findViewById(R.id.etLongitude);
        etTiket = (EditText) findViewById(R.id.etTiket);
        etFasilitas = (EditText) findViewById(R.id.etFasilitas);
        etBiayaTrans = (EditText) findViewById(R.id.etBiaya_transport);
        etKeamanan = (EditText) findViewById(R.id.etKeamanan);
        etVideo = (EditText) findViewById(R.id.etVideo);
        buttonSubmit = (Button) findViewById(R.id.buttonSubmitTempat);



        cvVideo = (CardView)findViewById(R.id.view2);

        buttonSubmit.setOnClickListener(this);
        etVideo.setOnClickListener(this);

        transparentImageView = (ImageView) findViewById(R.id.transparent_image);
        mainScrollView = (ScrollView) findViewById(R.id.scrollViewTempat);

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        handleIntent(getIntent());


    }

    private void handleIntent(Intent intent) {
        tempat = (Tempat)intent.getSerializableExtra("tempat");
        if(intent.getExtras() != null && tempat != null){
            isEditMode = true;
            daerah = (Daerah)intent.getSerializableExtra("daerah");

            etNamaTempat.setText(tempat.getNama_tempat());
            etlatitude.setText(String.valueOf(tempat.getLatitude()));
            etLongitude.setText(String.valueOf(tempat.getLongitude()));
            etTiket.setText(tempat.getTiket_masuk());
            etFasilitas.setText(tempat.getFasilitas());
            etBiayaTrans.setText(tempat.getBiaya_transportasi());
            etKeamanan.setText(tempat.getKeamanan());

            cvVideo.setVisibility(View.GONE);
            buttonSubmit.setText("Ubah");
        }else {
            daerah = (Daerah)intent.getSerializableExtra("daerah");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(isEditMode){
            getMenuInflater().inflate(R.menu.menu_form_tempat, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_delete){
            actionDelete();
        }
        return super.onOptionsItemSelected(item);
    }

    private void actionDelete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Yakin ingin menghapus data ini??");
        builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteTempat(dialog);
            }
        });
        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setIcon(R.drawable.ic_error);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void deleteTempat(DialogInterface dialog) {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);
        pd.show();

        MethodApi methodApi = RestApi.getRetrofit().create(MethodApi.class);
        methodApi.deleteTempat(tempat.getId_tempat(), new Callback<ResponseApi>() {
            @Override
            public void success(ResponseApi responseApi, Response response) {
                if (responseApi.isSuccess()) {
                    pd.dismiss();
                    DialogUtils.showInfoDialog(FormTempatActivity.this, "Data berhasil dihapus", new Callable() {
                        @Override
                        public Object call() throws Exception {
                            FormTempatActivity.this.setResult(RESULT_OK);
                            FormTempatActivity.this.finish();
                            return null;
                        }
                    });

                }
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                RestApi.handleError(FormTempatActivity.this, error);
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(isEditMode){
            LatLng cLatLng = new LatLng(tempat.getLatitude(), tempat.getLongitude());
            mMap.addMarker(new MarkerOptions().position(cLatLng).title(tempat.getNama_tempat()));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cLatLng, 11));

        }else{

            LatLng bolmong = AppConfig.LATLNG_DEFAULT;
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.addMarker(new MarkerOptions().position(bolmong).title("Marker in Bolmong"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bolmong, 9));


        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(latLng));
                etlatitude.setText(String.valueOf(latLng.latitude));
                etLongitude.setText(String.valueOf(latLng.longitude));
            }
        });

    }

    @Override
    public void onClick(View v) {
        if(v == buttonSubmit) {
            if(isEditMode){
               editTempat();
            }else{
               createTempat();
            }

        }
        if(v == etVideo){
            startActivityForResult(openImageIntent(), REQ_CODE_VIDEO);
        }
    }

    private void createTempat() {
        if(validate()){
            submitTempat();
        }
    }

    private void editTempat() {
        if(validateUpdate()){
            submitUpdateTempat();
        }
    }

    private void submitTempat() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);
        pd.show();




        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        MultipartTypedOutput multipart = new MultipartTypedOutput();
        multipart.addPart("nama_tempat", new TypedString(etNamaTempat.getText().toString()));
        multipart.addPart("latitude", new TypedString(etlatitude.getText().toString()));
        multipart.addPart("longitude", new TypedString(etLongitude.getText().toString()));
        multipart.addPart("tiket_masuk", new TypedString(etTiket.getText().toString()));
        multipart.addPart("fasilitas", new TypedString(etFasilitas.getText().toString()));
        multipart.addPart("biaya_transportasi", new TypedString(etBiayaTrans.getText().toString()));
        multipart.addPart("keamanan", new TypedString(etKeamanan.getText().toString()));
        if(videoPath != null)
            multipart.addPart("file", new TypedFile("application/octet-stream", new File(videoPath)));
        multipart.addPart("id_user", new TypedString(String.valueOf(user.getId_user())));
        multipart.addPart("id_daerah", new TypedString(String.valueOf(daerah.getId_daerah())));

        method.addTempat(multipart, new Callback<ResponseApi>() {
            @Override
            public void success(ResponseApi responseApi, Response response) {
                if(responseApi.isSuccess()){
                    if (BuildConfig.DEBUG) Log.d("FormTempatActivity", "Upload berhasil");
                    DialogUtils.showInfoDialog(FormTempatActivity.this, "Data berhasil diperbaharui", new Callable() {
                        @Override
                        public Object call() throws Exception {
                            FormTempatActivity.this.setResult(RESULT_OK);
                            FormTempatActivity.this.finish();
                            return null;
                        }
                    });

                }else{
                    if (BuildConfig.DEBUG) Log.d("FormTempatActivity", "gagal uploads");
                }

                pd.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                RestApi.handleError(FormTempatActivity.this, error);
            }
        });

    }

    private void submitUpdateTempat() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);
        pd.show();

        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        MultipartTypedOutput multipart = new MultipartTypedOutput();
        multipart.addPart("nama_tempat", new TypedString(etNamaTempat.getText().toString()));
        multipart.addPart("latitude", new TypedString(etlatitude.getText().toString()));
        multipart.addPart("longitude", new TypedString(etLongitude.getText().toString()));
        multipart.addPart("tiket_masuk", new TypedString(etTiket.getText().toString()));
        multipart.addPart("fasilitas", new TypedString(etFasilitas.getText().toString()));
        multipart.addPart("biaya_transportasi", new TypedString(etBiayaTrans.getText().toString()));
        multipart.addPart("keamanan", new TypedString(etKeamanan.getText().toString()));
        multipart.addPart("id_user", new TypedString(String.valueOf(user.getId_user())));
        multipart.addPart("id_daerah", new TypedString(String.valueOf(daerah.getId_daerah())));

        method.updateTempat(multipart, tempat.getId_tempat(), new Callback<ResponseApi>() {
            @Override
            public void success(ResponseApi responseApi, Response response) {
                if (responseApi.isSuccess()) {
                    if (BuildConfig.DEBUG) Log.d("FormTempatActivity", "update berhasil");
                    DialogUtils.showInfoDialog(FormTempatActivity.this, "Data berhasil diperbaharui", new Callable() {
                        @Override
                        public Object call() throws Exception {
                            if (BuildConfig.DEBUG)
                                Log.d("FormTempatActivity", "OK di click lewat callable");

                            FormTempatActivity.this.setResult(RESULT_OK);
                            FormTempatActivity.this.finish();

                            return null;
                        }
                    });


                } else {
                    if (BuildConfig.DEBUG) Log.d("FormTempatActivity", "gagal uploads");
                }

                pd.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                RestApi.handleError(FormTempatActivity.this, error);
            }
        });

    }

    private boolean validateUpdate() {

        EditText[] allEditText = {etlatitude, etLongitude, etNamaTempat, etTiket, etFasilitas, etKeamanan};
        for(EditText editText : allEditText){
            if(editText.getText().toString().equals("")){
                editText.setError("harus diisi");
                return false;
            }
        }

        return true;
    }

    private boolean validate() {

        EditText[] allEditText = {etlatitude, etLongitude, etNamaTempat, etTiket, etFasilitas, etKeamanan};
        for(EditText editText : allEditText){
            if(editText.getText().toString().equals("")){
                editText.setError("harus diisi");
                return false;
            }
        }

        return true;
    }

    private Intent openImageIntent() {


        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("video/*");
        galleryIntent.setAction(Intent.ACTION_PICK);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        return chooserIntent;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQ_CODE_VIDEO && resultCode == RESULT_OK) {
            if (BuildConfig.DEBUG) Log.d("FormTempatActivity", "Select Video");
            Uri selectedImageUri = data.getData();
            //videoPath = getPath(selectedImageUri);
            videoPath = getRealPathFromURI(this, selectedImageUri);
            File f = new File(videoPath);
            Log.d("FormTempatActivity", videoPath);
            etVideo.setText(f.getName());
        }
    }

    private String getPath(Uri selectedImageUri) {
        File file = new File(selectedImageUri.getPath());
        return file.getAbsolutePath();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
