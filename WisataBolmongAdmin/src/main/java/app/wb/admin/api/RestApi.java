package app.wb.admin.api;

import android.content.Context;
import android.widget.Toast;
import app.wb.admin.AppConfig;
import retrofit.RestAdapter;
import retrofit.RetrofitError;


public class RestApi {

    public static RestAdapter getRetrofit(){
        RestAdapter retrofit = new RestAdapter.Builder()
                .setEndpoint(AppConfig.API_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        return retrofit;
    }

    public static void handleError(Context context, RetrofitError error){
        if(error.getKind() == RetrofitError.Kind.NETWORK){
            Toast.makeText(context,"no internet connection", Toast.LENGTH_SHORT).show();
        }
    }

}
