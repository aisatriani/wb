package app.wb.admin.api;

import android.telecom.Call;

import java.util.List;
import java.util.Map;

import app.wb.admin.model.Daerah;
import app.wb.admin.model.Kecamatan;
import app.wb.admin.model.RekomendasiTempat;
import app.wb.admin.model.Tempat;
import app.wb.admin.model.Users;
import app.wb.admin.model.UsersAdmin;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.mime.MultipartTypedOutput;

/**
 * Created by aisatriani on 16/10/15.
 */

public interface MethodApi {

    @FormUrlEncoded
    @POST("/login")
    void login(@Field("username") String username, @Field("password") String password, Callback<ResponseApi> callback);

    @GET("/useradmin")
    void getUsersAdmin(Callback<ResultApi<UsersAdmin>> callback);

    @FormUrlEncoded
    @POST("/users")
    void addUsers(
            @Field("username") String username,
            @Field("password") String password,
            @Field("email") String email,
            @Field("nama_lengkap") String nama_lengkap,
            @Field("id_daerah") int id_daerah,
            Callback<ResponseApi> callback
    );

    @FormUrlEncoded
    @POST("/users/{id}")
    void updateUsers(
            @Path("id") int id,
            @Field("username") String username,
            @Field("password") String password,
            @Field("email") String email,
            @Field("nama_lengkap") String nama_lengkap,
            @Field("id_daerah") int id_daerah,
            Callback<ResponseApi> callback);

    @GET("/users/{id}")
    void getUsers(@Path("id") int id, Callback<ResultApi<Users>> callback);

    @GET("/users")
    void getUsers(Callback<ResultApi<Users>> callback);

    @DELETE("/users/{id}")
    void deleteUsers(@Path("id") int id, Callback<ResponseApi> callback);

    @GET("/daerah")
    void getDaerah(Callback<ResultApi<Daerah>> callback);

    @FormUrlEncoded
    @POST("/daerah")
    void addDaerah(@Field("nama_daerah") String nama_daerah, Callback<ResponseApi> callback);

    @FormUrlEncoded
    @POST("/daerah/{id}")
    void updateDaerah(@Path("id") int id, @Field("nama_daerah") String nama_daerah);

    @DELETE("/daerah/{id}")
    void deleteDaerah(@Path("id") int id, Callback<ResponseApi> callback);

    @FormUrlEncoded
    @POST("/kecamatan")
    void addKecamatan(@Field("nama_kecamatan") String nm, @Field("id_daerah") int id_daerah, Callback<ResponseApi> callback);

    @GET("/kecamatan/daerah/{id}")
    void getKecamatanByDaerah(@Path("id") int id_daerah, Callback<ResultApi<Kecamatan>> callback);

    @GET("/tempat")
    void getTempat(Callback<ResultApi<Tempat>> callback);

    @POST("/tempat")
    void addTempat(@Body MultipartTypedOutput part, Callback<ResponseApi> callback);

    @POST("/tempat/{id}")
    void updateTempat(@Body MultipartTypedOutput part, @Path("id") int id, Callback<ResponseApi> callback);

    @DELETE("/tempat/{id}")
    void deleteTempat(@Path("id") int id, Callback<ResponseApi> callback);

    @GET("/tempat/daerah/{id}")
    void getTempatByDaerah(@Path("id")int id_daerah, Callback<ResultApi<Tempat>> callback);

    @GET("/tempat/recomended/{id}")
    void getTempatRecomended(@Path("id") int id_daerah, Callback<ResultApi<RekomendasiTempat>> callback);

    @FormUrlEncoded
    @POST("/tempat/recomended/{id}")
    void storeTempatRecomended(@Path("id") int id_daerah1, @FieldMap Map<String, String> fieldMap, Callback<ResponseApi> callback );

    @POST("/tempat/recomended/{id}")
    void storeRecomended(@Path("id") int id_daerah1, @Body List<String> nama_tempat, Callback<ResponseApi> callback );

    @FormUrlEncoded
    @POST("/recomended/{id}")
    void tambahRekomendasi(@Path("id") int id_daerah, @Field("nama_tempat") String nama_tempat, @Field("keterangan") String keterangan, Callback<ResultApi<RekomendasiTempat>> callback);

    @DELETE("/recomended/{id}")
    void deleteTempatRekomendasi(@Path("id") int id, Callback<ResponseApi> callback);

    @FormUrlEncoded
    @POST("/recomended/{id}/edit")
    void updateTempatRekom(@Path("id") int id_tr, @Field("nama_tempat") String nama_tempat, @Field("keterangan") String keterangan, Callback<ResponseApi> callback);

}
