package app.wb.admin.api;

/**
 * Created by aisatriani on 17/10/15.
 */
public class ResponseApi {

    private String action;
    private boolean success;
    private int id;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
