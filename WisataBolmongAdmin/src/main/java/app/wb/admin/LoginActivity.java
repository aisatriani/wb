package app.wb.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import app.wb.admin.api.MethodApi;
import app.wb.admin.api.ResponseApi;
import app.wb.admin.api.RestApi;
import app.wb.admin.api.ResultApi;
import app.wb.admin.model.Users;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private EditText txtUsername;
    private EditText txtPassword;
    private Button btnLogin;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        txtUsername = (EditText)findViewById(R.id.edUsername);
        txtPassword = (EditText) findViewById(R.id.edPassword);
        btnLogin = (Button) findViewById(R.id.buttonLogin);
        btnLogin.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id =  v.getId();
        switch (id){
            case R.id.buttonLogin:
                Log.d("LoginActivity", "handle button login");
                if(validate()){
                    submitLogin();
                }
                break;
        }
    }

    private void submitLogin() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.show();

        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        method.login(txtUsername.getText().toString(), txtPassword.getText().toString(), new Callback<ResponseApi>() {
            @Override
            public void success(ResponseApi responseApi, Response response) {

                if(responseApi.isSuccess()){
                    if (BuildConfig.DEBUG) Log.d("LoginActivity", "login berhasil");
                    getUserLogin(responseApi.getId());

                    //Intent
                }else{
                    pd.dismiss();
                    if (BuildConfig.DEBUG) Log.d("LoginActivity", "login gagal");
                }
            }

            private void getUserLogin(int id) {
                MethodApi methodApi = RestApi.getRetrofit().create(MethodApi.class);
                methodApi.getUsers(id, new Callback<ResultApi<Users>>() {
                    @Override
                    public void success(ResultApi<Users> usersResultApi, Response response) {
                        pd.dismiss();
                        List<Users> usersList = usersResultApi.getItems();
                        for(Users users : usersList){
                            Preference.setUserData(LoginActivity.this, users);
                        }
                        Preference.setLogin(LoginActivity.this, true);

                        Intent intent = new Intent(LoginActivity.this, MapsActivity.class);
                        startActivity(intent);
                        LoginActivity.this.finish();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pd.dismiss();
                        RestApi.handleError(LoginActivity.this, error);
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                RestApi.handleError(LoginActivity.this, error);
            }
        });


    }

    private boolean validate() {

        EditText[] editTexts = {txtUsername, txtPassword};
        for(EditText editText : editTexts){
            if(editText.getText().toString().equals("")) {
                editText.setError("tidak boleh kosong");
                return false;
            }
        }
        return true;
    }
}
