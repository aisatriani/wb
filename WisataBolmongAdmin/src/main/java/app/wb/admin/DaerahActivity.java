package app.wb.admin;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import app.wb.admin.adapter.DaerahAdapter;
import app.wb.admin.api.MethodApi;
import app.wb.admin.api.ResponseApi;
import app.wb.admin.api.RestApi;
import app.wb.admin.api.ResultApi;
import app.wb.admin.model.Daerah;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class DaerahActivity extends BaseActivity implements View.OnClickListener {

    private ListView listViewDaerah;
    private FloatingActionButton fab;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_daerah;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listViewDaerah = (ListView) findViewById(R.id.listViewDaerah);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.attachToListView(listViewDaerah);
        fab.setOnClickListener(this);

        loadDaerah();
    }

    private void loadDaerah() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.show();

        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        method.getDaerah(new Callback<ResultApi<Daerah>>() {
            @Override
            public void success(ResultApi<Daerah> daerahResultApi, Response response) {
                if (daerahResultApi.isStatus()) {
                    final DaerahAdapter adapter = new DaerahAdapter(DaerahActivity.this, daerahResultApi.getItems());
                    listViewDaerah.setAdapter(adapter);
                    listViewDaerah.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Daerah daerah = adapter.getItem(position);
                            Intent intent = new Intent(DaerahActivity.this, TempatWisataActivity.class);
                            intent.putExtra("daerah",daerah);
                            startActivity(intent);
                        }
                    });
                    pd.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                RestApi.handleError(DaerahActivity.this, error);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_daerah, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.fab){
            createFormDialog();
        }
    }

    private void createFormDialog() {
        final EditText editText = new EditText(this);
        editText.setHint("nama daerah");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Form Daerah");
        builder.setView(editText);
        builder.setPositiveButton("Tambahkan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText.getText().toString().equals("")) {
                    editText.setError("tidak boleh kosong");
                }else{
                    MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
                    method.addDaerah(editText.getText().toString(), new Callback<ResponseApi>() {
                        @Override
                        public void success(ResponseApi responseApi, Response response) {
                            if(responseApi.isSuccess()){
                                loadDaerah();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            RestApi.handleError(DaerahActivity.this, error);
                        }
                    });
                }
            }
        });
    }
}
