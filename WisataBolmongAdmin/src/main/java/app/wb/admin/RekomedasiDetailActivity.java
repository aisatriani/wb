package app.wb.admin;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import app.wb.admin.api.MethodApi;
import app.wb.admin.api.ResponseApi;
import app.wb.admin.api.RestApi;
import app.wb.admin.model.RekomendasiTempat;
import app.wb.admin.utils.DialogUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RekomedasiDetailActivity extends BaseActivity implements View.OnClickListener {

    private EditText ed_nama_tempat;
    private EditText ed_deskripsi;
    private Button button_simpan;
    private RekomendasiTempat rt;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_rekomedasi_detail;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ed_nama_tempat = (EditText) findViewById(R.id.edNamaTempatR);
        ed_deskripsi = (EditText) findViewById(R.id.edDeskripsiR);
        button_simpan = (Button) findViewById(R.id.btn_simpan_R);

        button_simpan.setOnClickListener(this);

        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {

        rt = (RekomendasiTempat) intent.getSerializableExtra("rt");
        ed_nama_tempat.setText(rt.getNama_tempat());
        ed_deskripsi.setText(rt.getKeterangan());

        System.out.println("id rt :" +rt.getId_tr());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form_tempat, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_delete){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Yakin Ingin menghapus data ini?");
            builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    actionDelete();
                }
            });
            builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

        }
        return super.onOptionsItemSelected(item);
    }

    private void actionDelete() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Loading");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        MethodApi methodApi = RestApi.getRetrofit().create(MethodApi.class);
        methodApi.deleteTempatRekomendasi(rt.getId_tr(), new Callback<ResponseApi>() {
            @Override
            public void success(ResponseApi responseApi, Response response) {
                pd.dismiss();
                Toast.makeText(RekomedasiDetailActivity.this, "Berhasil DIhapus",Toast.LENGTH_LONG).show();
                setResult(RESULT_OK);
                RekomedasiDetailActivity.this.finish();
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                DialogUtils.showInfoDialog(RekomedasiDetailActivity.this, "Gagal menghapus data");
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_simpan_R){
            updateTempatRekomendasi();
        }
    }

    private void updateTempatRekomendasi() {
        if(ed_nama_tempat.getText().toString().equals("")){
            ed_nama_tempat.setError("tidak boleh kosong");
            return;
        }
        if(ed_deskripsi.getText().toString().equals("")){
            ed_deskripsi.setError("tidak boleh kosong");
            return;
        }

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        MethodApi methodApi = RestApi.getRetrofit().create(MethodApi.class);
        methodApi.updateTempatRekom(rt.getId_tr(), ed_nama_tempat.getText().toString(), ed_deskripsi.getText().toString(), new Callback<ResponseApi>() {
            @Override
            public void success(ResponseApi responseApi, Response response) {
                pd.dismiss();
                Toast.makeText(RekomedasiDetailActivity.this, "Data Berhasil diubah",Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                DialogUtils.showInfoDialog(RekomedasiDetailActivity.this, "Gagal Memproses data. Harap Ulangi kembali");
            }
        });
    }
}
