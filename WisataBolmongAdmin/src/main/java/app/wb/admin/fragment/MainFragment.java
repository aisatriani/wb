package app.wb.admin.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.wb.admin.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {


    private TextView fitur_list;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main_admin, container, false);
        fitur_list = (TextView)v.findViewById(R.id.welcome_fitur_text);

        String html = "<ul>" +
                "<li>Menampilkan Wisata Dalam Bentuk pemetaan digital</li>" +
                "<li>Fitur jarak dan waktu tempuh dari tujuan asal ke obyek wisata dengan otomatis</li>" +
                "</ul>";
        Spanned htmlList = Html.fromHtml(html);
        fitur_list.setText(htmlList);
        return v;
    }

}
