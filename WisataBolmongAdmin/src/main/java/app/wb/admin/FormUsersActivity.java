package app.wb.admin;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import app.wb.admin.api.MethodApi;
import app.wb.admin.api.ResponseApi;
import app.wb.admin.api.RestApi;
import app.wb.admin.api.ResultApi;
import app.wb.admin.model.Daerah;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FormUsersActivity extends BaseActivity implements View.OnClickListener {

    private EditText etUsername;
    private EditText etPassword;
    private EditText etRePassword;
    private EditText etEmail;
    private EditText etNamaLengkap;
    private Button btnSubmit;
    private Spinner spinDaerah;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_form_users;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etRePassword = (EditText) findViewById(R.id.etRePassword);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etNamaLengkap = (EditText) findViewById(R.id.etNamaLengkap);
        spinDaerah = (Spinner) findViewById(R.id.spinDaerah);
        btnSubmit = (Button) findViewById(R.id.buttonSubmit);
        btnSubmit.setOnClickListener(this);

        loadDaerahToSpinner();

    }

    private void loadDaerahToSpinner() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.show();

        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        method.getDaerah(new Callback<ResultApi<Daerah>>() {
            @Override
            public void success(ResultApi<Daerah> daerahResultApi, Response response) {
                ArrayAdapter<Daerah> adapter = new ArrayAdapter<Daerah>(FormUsersActivity.this,android.R.layout.simple_spinner_dropdown_item, daerahResultApi.getItems());
                spinDaerah.setAdapter(adapter);
                pd.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                RestApi.handleError(FormUsersActivity.this, error);
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.buttonSubmit:
                if(validate()){
                    submitProses();
                }
                break;
        }
    }

    private void submitProses() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.show();
        Daerah daerah = (Daerah)spinDaerah.getSelectedItem();
        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        method.addUsers(
                etUsername.getText().toString(),
                etPassword.getText().toString(),
                etEmail.getText().toString(),
                etNamaLengkap.getText().toString(),
                daerah.getId_daerah(),
                new Callback<ResponseApi>() {

            @Override
            public void success(ResponseApi responseApi, Response response) {
                pd.dismiss();
                if(responseApi.isSuccess()){
                    setResult(RESULT_OK);
                    FormUsersActivity.this.finish();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                RestApi.handleError(FormUsersActivity.this, error);
            }
        });
    }

    private boolean validate() {
        EditText[] editTexts = {etUsername, etPassword, etRePassword, etEmail, etNamaLengkap};
        for(EditText ed : editTexts){
            if(ed.getText().toString().equals("")){
                ed.setError("tidak boleh kosong");
                return false;
            }
        }

        if(!etPassword.getText().toString().equals(etRePassword.getText().toString())){
            etRePassword.setError("password harus dikonfirmasi");
            return false;
        }

        return true;
    }
}
