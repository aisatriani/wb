package app.wb.admin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FormDaerahActivity extends BaseActivity {

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_form_daerah;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
