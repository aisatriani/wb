package app.wb.admin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import app.wb.admin.api.MethodApi;
import app.wb.admin.api.ResponseApi;
import app.wb.admin.api.RestApi;
import app.wb.admin.api.ResultApi;
import app.wb.admin.model.Daerah;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private EditText username, password, repassword, email, nama;
    private Button btnSubmit;
    private ProgressDialog pd;
    private Spinner spinDaerah;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        repassword = (EditText)findViewById(R.id.repassword);
        email = (EditText)findViewById(R.id.email);
        nama = (EditText)findViewById(R.id.nama_lengkap);
        spinDaerah = (Spinner)findViewById(R.id.spinnerDaerah);


        btnSubmit = (Button)findViewById(R.id.buttonSubmit);
        btnSubmit.setOnClickListener(this);

        loadDaerah();

    }

    private void loadDaerah() {
        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.load_daerah));
        pd.show();
        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        method.getDaerah(new Callback<ResultApi<Daerah>>() {
            @Override
            public void success(ResultApi<Daerah> daerahResultApi, Response response) {
                closeProgress();
                ArrayAdapter<Daerah> adapter = new ArrayAdapter<Daerah>(RegisterActivity.this,android.R.layout.simple_spinner_dropdown_item, daerahResultApi.getItems());
                spinDaerah.setAdapter(adapter);
            }

            @Override
            public void failure(RetrofitError error) {
                closeProgress();
                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                builder.setMessage(getString(R.string.error_retrieving_data));
                builder.setCancelable(false);
                builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        loadDaerah();
                    }
                });
                builder.setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        RegisterActivity.this.finish();
                    }
                });
                builder.show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.buttonSubmit){
            if(validateOnSubmit()){
                submitData();
            }

        }
    }

    private boolean validateOnSubmit() {
        boolean val = false;
        EditText[] editTexts = {username , password, repassword, email, nama };
        for(EditText ed : editTexts){
            if(ed.getText().toString().equals("")){
                ed.setError(getString(R.string.error_field_required));
                val = false;
            }else{
                val = true;
            }

        }

        return val;
    }

    private void submitData() {
        pd = new ProgressDialog(this);
        pd.setMessage("please wait");
        pd.show();
        Daerah daerah = (Daerah)spinDaerah.getSelectedItem();
        MethodApi methodApi = RestApi.getRetrofit().create(MethodApi.class);
        methodApi.addUsers(
                username.getText().toString(),
                password.getText().toString(),
                email.getText().toString(),
                nama.getText().toString(),
                daerah.getId_daerah(),
                new Callback<ResponseApi>() {
                    @Override
                    public void success(ResponseApi responseApi, Response response) {
                        closeProgress();
                        if(responseApi.isSuccess()){
                            System.out.println("register completed");
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        closeProgress();
                        RestApi.handleError(RegisterActivity.this, error);
                    }
                }
        );
    }

    public void closeProgress(){
        if(pd != null){
            if(pd.isShowing()){
                pd.dismiss();
            }
        }
    }
}
