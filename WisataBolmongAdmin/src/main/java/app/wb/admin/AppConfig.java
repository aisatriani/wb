package app.wb.admin;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by aisatriani on 16/10/15.
 */
public class AppConfig {
    //theatering usb/wifi
    //public static final String API_URL = "http://192.168.42.114/wisatabolmong";
    //local wifi connection
    //public static final String API_URL = "http://192.168.2.128/wisatabolmong";
    public static final String API_URL = "http://wb.tenilodev.com";
    public static final LatLng LATLNG_DEFAULT = new LatLng(0.620487, 123.852310);

    public static final int RES_CODE_EDIT = 88;
    public static final int RES_CODE_DELETE = 99;
}
