package app.wb.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.wb.admin.api.MethodApi;
import app.wb.admin.api.ResponseApi;
import app.wb.admin.api.RestApi;
import app.wb.admin.api.ResultApi;
import app.wb.admin.model.RekomendasiTempat;
import app.wb.admin.utils.DialogUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RekomendasiActivity extends BaseActivity implements View.OnClickListener {

    private List<EditText> editTextList = new ArrayList<>();
    private int id_daerah;
    private LinearLayout linearLayout;
    private ListView list;
    private int data;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_rekomendasi;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        linearLayout = (LinearLayout)findViewById(R.id.linear_edittext);

        id_daerah = getIntent().getIntExtra("id_daerah",0);
        list = (ListView) findViewById(R.id.list_rekomendasi);


        loadRekomendasi();


    }

    @Override
    protected void onResume() {
        super.onResume();
        loadRekomendasi();
    }

    private void loadRekomendasi() {
        //linearLayout.removeAllViews();
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.show();

        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        method.getTempatRecomended(id_daerah, new Callback<ResultApi<RekomendasiTempat>>() {
            @Override
            public void success(ResultApi<RekomendasiTempat> rekomendasiTempatResultApi, Response response) {
                pd.dismiss();
//                int i = 0;
//                for(RekomendasiTempat rekom : rekomendasiTempatResultApi.getItems()){
//                    linearLayout.addView(editText(i, rekom.getNama_tempat()));
//                    i++;
//                }

                List<RekomendasiTempat> listr = rekomendasiTempatResultApi.getItems();


                final ArrayAdapter<RekomendasiTempat> adapter = new ArrayAdapter<RekomendasiTempat>(RekomendasiActivity.this,android.R.layout.simple_list_item_1,listr);
                list.setAdapter(adapter);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        RekomendasiTempat rt = adapter.getItem(position);
                        Intent i = new Intent(RekomendasiActivity.this, RekomedasiDetailActivity.class);
                        i.putExtra("rt", rt);
                        startActivityForResult(i, 200);
                    }
                });

            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
            }
        });
    }

    private EditText editText(int id, String hint) {
        EditText editText = new EditText(this);
        //editText.setId(id);
        editText.setText(hint);
        editTextList.add(editText);
        return editText;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_intro,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_addtext){
            //linearLayout.addView(editText(1, ""));
            showFormDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFormDialog() {
        View view = getLayoutInflater().inflate(R.layout.layout_dialog_form_rekomendasi,null);
        final EditText edTitle = (EditText)view.findViewById(R.id.edNamaTempatRekomendasi);
        final EditText edDeskripsi = (EditText)view.findViewById(R.id.edRekomDeskripsi);
        Button btn_tambah = (Button)view.findViewById(R.id.btn_submit_rekom);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();

        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edTitle.getText().toString().equals("")){
                    edTitle.setError("tidak boleh kosong");
                    return;
                }
                if(edDeskripsi.getText().toString().equals("")){
                    edDeskripsi.setError("tidal boleh kosong");
                    return;
                }

                dialog.dismiss();
                final ProgressDialog pd = new ProgressDialog(RekomendasiActivity.this);
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("Loading..");
                pd.show();

                MethodApi methodApi = RestApi.getRetrofit().create(MethodApi.class);
                methodApi.tambahRekomendasi(id_daerah, edTitle.getText().toString(), edDeskripsi.getText().toString(), new Callback<ResultApi<RekomendasiTempat>>() {
                    @Override
                    public void success(ResultApi<RekomendasiTempat> rekomendasiTempatResultApi, Response response) {
                        pd.dismiss();
                        //DialogUtils.showInfoDialog(RekomendasiActivity.this, "Data Berhasil ditambahkan");
                        Toast.makeText(RekomendasiActivity.this, "Data berhasil Ditambahkan", Toast.LENGTH_LONG).show();
                        loadRekomendasi();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pd.dismiss();
                        DialogUtils.showInfoDialog(RekomendasiActivity.this, "Error..! Terjadi kesalahan saat memproses data. Harap Ulangi kembali");
                    }
                });
            }
        });

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button_simpan){
            submit();
        }
    }

    private void submit() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.show();

        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        Map<String, String> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        for(EditText et : editTextList){
            if(!et.getText().toString().equals(""))
                list.add(et.getText().toString());
        }

        method.storeRecomended(id_daerah, list, new Callback<ResponseApi>() {
            @Override
            public void success(ResponseApi responseApi, Response response) {
                pd.dismiss();
                if(response.getStatus() == 200){
                    Toast.makeText(RekomendasiActivity.this, "data berhasil diperbaharui",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                Toast.makeText(RekomendasiActivity.this, "Error....!",Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == 200){
            loadRekomendasi();
        }
    }
}
