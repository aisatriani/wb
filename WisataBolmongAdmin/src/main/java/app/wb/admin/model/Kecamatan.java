package app.wb.admin.model;

import java.io.Serializable;

/**
 * Created by aisatriani on 14/02/16.
 */
public class Kecamatan implements Serializable {
    private int id_kecamatan;
    private String nama_kecamatan;
    private Daerah daerah;

    public int getId_kecamatan() {
        return id_kecamatan;
    }

    public void setId_kecamatan(int id_kecamatan) {
        this.id_kecamatan = id_kecamatan;
    }

    public String getNama_kecamatan() {
        return nama_kecamatan;
    }

    public void setNama_kecamatan(String nama_kecamatan) {
        this.nama_kecamatan = nama_kecamatan;
    }

    public Daerah getDaerah() {
        return daerah;
    }

    public void setDaerah(Daerah daerah) {
        this.daerah = daerah;
    }
}
