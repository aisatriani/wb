package app.wb.admin.model;

import java.io.Serializable;

/**
 * Created by aisatriani on 14/02/16.
 */
public class Tempat implements Serializable {
    private int id_tempat;
    private String nama_tempat;
    private double latitude;
    private double longitude;
    private String tiket_masuk;
    private String fasilitas;
    private String biaya_transportasi;
    private String keamanan;
    private String video_url;
    private int created_at;
    private Users pengguna;
    private Daerah daerah;

    public int getId_tempat() {
        return id_tempat;
    }

    public void setId_tempat(int id_tempat) {
        this.id_tempat = id_tempat;
    }

    public String getNama_tempat() {
        return nama_tempat;
    }

    public void setNama_tempat(String nama_tempat) {
        this.nama_tempat = nama_tempat;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTiket_masuk() {
        return tiket_masuk;
    }

    public void setTiket_masuk(String tiket_masuk) {
        this.tiket_masuk = tiket_masuk;
    }

    public String getFasilitas() {
        return fasilitas;
    }

    public void setFasilitas(String fasilitas) {
        this.fasilitas = fasilitas;
    }

    public String getKeamanan() {
        return keamanan;
    }

    public void setKeamanan(String keamanan) {
        this.keamanan = keamanan;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public int getCreated_at() {
        return created_at;
    }

    public void setCreated_at(int created_at) {
        this.created_at = created_at;
    }

    public Users getPengguna() {
        return pengguna;
    }

    public void setPengguna(Users pengguna) {
        this.pengguna = pengguna;
    }

    public Daerah getDaerah() {
        return daerah;
    }

    public void setDaerah(Daerah daerah) {
        this.daerah = daerah;
    }

    public String getBiaya_transportasi() {
        return biaya_transportasi;
    }

    public void setBiaya_transportasi(String biaya_transportasi) {
        this.biaya_transportasi = biaya_transportasi;
    }
}
