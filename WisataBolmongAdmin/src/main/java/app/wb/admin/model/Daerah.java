package app.wb.admin.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by aisatriani on 16/10/15.
 */
public class Daerah implements Serializable {
    private int id_daerah;
    private String nama_daerah;
    private List<Kecamatan> kecamatan;

    public int getId_daerah() {
        return id_daerah;
    }

    public void setId_daerah(int id_daerah) {
        this.id_daerah = id_daerah;
    }

    public String getNama_daerah() {
        return nama_daerah;
    }

    public void setNama_daerah(String nama_daerah) {
        this.nama_daerah = nama_daerah;
    }

    public List<Kecamatan> getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(List<Kecamatan> kecamatan) {
        this.kecamatan = kecamatan;
    }

    @Override
    public String toString() {
        return getNama_daerah();
    }
}
