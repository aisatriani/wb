package app.wb.admin;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

/**
 * Created by aisatriani on 16/10/15.
 */
public abstract class BaseActivity extends AppCompatActivity implements Drawer.OnDrawerItemClickListener {

    private Toolbar toolbar;
    private Drawer result;

    private int selected_position;
    private final static String selected_tag = "selected_pos";
    public final static int IDENT_HOME = 1;
    public final static int IDENT_USERS = 2;
    public final static int IDENT_DAERAH = 3;
    public final static int IDENT_TEMPAT = 4;
    public final static int IDENT_LOGOUT = 101;


    protected abstract int getLayoutResource();

    protected abstract boolean isActivityAsUpHome();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        toolbar = (Toolbar)findViewById(R.id.toolbar);

        if(toolbar != null){
            setSupportActionBar(toolbar);
            if(isActivityAsUpHome()){
                ActionBar ab = getSupportActionBar();
                ab.setDisplayHomeAsUpEnabled(true);
            }else{
                initDrawer();
            }
        }
    }

    private void initDrawer() {
        PrimaryDrawerItem item1 = new PrimaryDrawerItem()
                .withIcon(FontAwesome.Icon.faw_home)
                .withIdentifier(IDENT_HOME)
                .withName("HOME");

        PrimaryDrawerItem itemUser = new PrimaryDrawerItem()
                .withIcon(FontAwesome.Icon.faw_user)
                .withIdentifier(IDENT_USERS)
                .withName("USERS");

        PrimaryDrawerItem itemDaerah = new PrimaryDrawerItem()
                .withIcon(FontAwesome.Icon.faw_bars)
                .withIdentifier(IDENT_DAERAH)
                .withName("TEMPAT WISATA");

//        PrimaryDrawerItem itemTempat = new PrimaryDrawerItem()
//                .withIcon(FontAwesome.Icon.faw_tripadvisor)
//                .withIdentifier(IDENT_TEMPAT)
//                .withName("TEMPAT WISATA");

        AccountHeader accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(true)
                .withSelectionFirstLine("WISATA BOLMONG")
                .addProfiles(new ProfileDrawerItem()
                                .withIcon(FontAwesome.Icon.faw_user)
                                .withName("Moh kharis")
                                .withNameShown(true)
                                .withEmail("admin@gmail.com")

                )
                .withCurrentProfileHiddenInList(true)
                .withAlternativeProfileHeaderSwitching(false)
                .withHeaderBackground(R.drawable.materialbg)
                .build();

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(accountHeader,true)
                .addDrawerItems(item1, itemUser, itemDaerah)
                .withOnDrawerItemClickListener(this)
                .addStickyDrawerItems(new SecondaryDrawerItem()
                        .withName("LOGOUT")
                        .withIdentifier(IDENT_LOGOUT)
                        .withIcon(FontAwesome.Icon.faw_sign_out)
                        )
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(result != null){
            result.setSelectionAtPosition(selected_position);
            result.closeDrawer();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(selected_tag, selected_position);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        selected_position = savedInstanceState.getInt(selected_tag);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if(result != null) {
            if (result.isDrawerOpen()) {
                result.closeDrawer();
                return;
            }
        }

        super.onBackPressed();
    }

    @Override
    public boolean onItemClick(View view, int i, IDrawerItem iDrawerItem) {
        return false;
    }

    public int getSelected_position() {
        return selected_position;
    }
}
