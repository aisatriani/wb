package app.wb.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.melnykov.fab.FloatingActionButton;

import app.wb.admin.adapter.TempatAdapter;
import app.wb.admin.api.MethodApi;
import app.wb.admin.api.RestApi;
import app.wb.admin.api.ResultApi;
import app.wb.admin.model.Daerah;
import app.wb.admin.model.Tempat;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class TempatWisataActivity extends BaseActivity {

    private static final int REQ_CODE = 201;
    private static final int REQ_CODE_EDIT = 202;
    private ListView listTempat;
    private FloatingActionButton fab;
    private Daerah daerah;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_tempat_wisata;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listTempat = (ListView) findViewById(R.id.listViewTempat);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.attachToListView(listTempat);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TempatWisataActivity.this, FormTempatActivity.class);
                intent.putExtra("daerah",daerah);
                startActivityForResult(intent, REQ_CODE);
            }
        });

        handleIntent(getIntent());

        //loadTempat();
    }

    private void handleIntent(Intent intent) {
        if(intent.getExtras() != null){
            daerah = (Daerah)intent.getSerializableExtra("daerah");
            loadTempat();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQ_CODE && resultCode == RESULT_OK){
            loadTempat();
        }
        if(requestCode == REQ_CODE_EDIT && resultCode == RESULT_OK){
            loadTempat();
        }
    }

    private void loadTempat() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.show();

        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        method.getTempatByDaerah(daerah.getId_daerah(), new Callback<ResultApi<Tempat>>() {
            @Override
            public void success(ResultApi<Tempat> tempatResultApi, Response response) {
                if(tempatResultApi.isStatus()){
                    final TempatAdapter adapter = new TempatAdapter(TempatWisataActivity.this, tempatResultApi.getItems());
                    listTempat.setAdapter(adapter);
                    listTempat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Tempat tempat = adapter.getItem(position);

                            Intent intent = new Intent(TempatWisataActivity.this, FormTempatActivity.class);
                            intent.putExtra("daerah",daerah);
                            intent.putExtra("tempat",tempat);
                            startActivityForResult(intent, REQ_CODE_EDIT);
                        }
                    });
                    pd.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                RestApi.handleError(TempatWisataActivity.this, error);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tempat_wisata, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_rekom) {
            Intent intent = new Intent(this, RekomendasiActivity.class);
            intent.putExtra("id_daerah",daerah.getId_daerah());
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
