package app.wb.admin;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.melnykov.fab.FloatingActionButton;

import app.wb.admin.adapter.KecamatanAdapter;
import app.wb.admin.api.MethodApi;
import app.wb.admin.api.ResponseApi;
import app.wb.admin.api.RestApi;
import app.wb.admin.api.ResultApi;
import app.wb.admin.model.Daerah;
import app.wb.admin.model.Kecamatan;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class KecamatanActivity extends BaseActivity {

    private Daerah daerah;
    private ListView listKecamatan;
    private FloatingActionButton fab;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_kecamatan;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Kecamatan");
        listKecamatan = (ListView) findViewById(R.id.listViewKecamatan);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.attachToListView(listKecamatan);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createFormDialog();
            }
        });
        handleIntent(getIntent());

    }

    private void loadKecamatanTolist(){

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon tunggu..");
        pd.show();

        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        method.getKecamatanByDaerah(daerah.getId_daerah(), new Callback<ResultApi<Kecamatan>>() {
            @Override
            public void success(ResultApi<Kecamatan> kecamatanResultApi, Response response) {
                KecamatanAdapter adapter = new KecamatanAdapter(KecamatanActivity.this, kecamatanResultApi.getItems());
                listKecamatan.setAdapter(adapter);
                pd.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                RestApi.handleError(KecamatanActivity.this, error);
            }
        });
    }

    private void createFormDialog() {
        final EditText editText = new EditText(this);
        editText.setHint("nama kecamatan");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Form Daerah");
        builder.setView(editText);
        builder.setPositiveButton("Tambahkan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().equals("")) {
                    editText.setError("tidak boleh kosong");
                } else {
                    submitKecamatan(dialog, editText.getText().toString(), daerah.getId_daerah());
                }
            }
        });
    }

    private void submitKecamatan(final AlertDialog dialog, String nama_kecamatan, int id_daerah) {
        MethodApi method = RestApi.getRetrofit().create(MethodApi.class);
        method.addKecamatan(nama_kecamatan, id_daerah, new Callback<ResponseApi>() {
            @Override
            public void success(ResponseApi responseApi, Response response) {
                if(responseApi.isSuccess()){
                    dialog.dismiss();
                    loadKecamatanTolist();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                RestApi.handleError(KecamatanActivity.this, error);
            }
        });
    }

    private void handleIntent(Intent intent) {
        if(intent.getExtras() != null){
            daerah = (Daerah)intent.getSerializableExtra("daerah");
            loadKecamatanTolist();
            //KecamatanAdapter adapter = new KecamatanAdapter(this, daerah.getKecamatan());
            //listKecamatan.setAdapter(adapter);
        }
    }
}
