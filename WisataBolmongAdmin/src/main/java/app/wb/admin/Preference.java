package app.wb.admin;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import app.wb.admin.model.Users;

/**
 * Created by aisatriani on 17/10/15.
 */
public class Preference {
    private static final String PREF_NAME = "default_preference";
    private static final String PREF_ISLOGIN = "pref_islogin";
    private static final String PREF_USERDATA = "pref_userdata";

    public static void setLogin(Context context, boolean val){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_ISLOGIN, val);
        editor.commit();
    }

    public static boolean isLogin(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        boolean islogin = sharedPreferences.getBoolean(PREF_ISLOGIN, false);
        return islogin;
    }

    public static void setUserData(Context context, Users users){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String userdata = new Gson().toJson(users);
        editor.putString(PREF_USERDATA, userdata);
        editor.commit();
    }

    public static Users getUserData(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        String jsonData = sharedPreferences.getString(PREF_USERDATA, null);
        Users users = new Gson().fromJson(jsonData,Users.class);
        return users;
    }
}
