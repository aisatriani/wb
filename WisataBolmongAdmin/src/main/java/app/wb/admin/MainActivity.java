package app.wb.admin;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import app.wb.admin.fragment.MainFragment;
import app.wb.admin.fragment.MapFragment;
import app.wb.admin.model.Daerah;
import app.wb.admin.model.Users;

import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

public class MainActivity extends BaseActivity {

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean isActivityAsUpHome() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Users users = Preference.getUserData(this);
        if (BuildConfig.DEBUG) Log.d("MainActivity","test user login " + users.getNama_lengkap());

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new MainFragment())
                    .commit();
        }else{
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new MainFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onItemClick(View view, int i, IDrawerItem iDrawerItem) {

        Log.i("drawer item click", String.valueOf(i));
        Log.i("drawer identifier click", String.valueOf(iDrawerItem.getIdentifier()));

        if(i != getSelected_position()){
            if(iDrawerItem.getIdentifier() == IDENT_HOME){
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new MainFragment())
                        .commit();
            }
            if(iDrawerItem.getIdentifier() == IDENT_USERS){
                startActivity(new Intent(this,UserActivity.class));
            }
            if(iDrawerItem.getIdentifier() == IDENT_DAERAH){
                startActivity(new Intent(this, DaerahActivity.class));
            }

        }

        return super.onItemClick(view, i, iDrawerItem);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }
}
